package com.application.banque.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.application.banque.associer.AssocierImplException;
import com.application.banque.associer.service.AssocierService;
import com.application.banque.client.ClientImplException;
import com.application.banque.client.service.ClientService;
import com.application.banque.compte.CompteImplException;
import com.application.banque.compte.service.CompteService;
import com.application.banque.produit.ProduitImplException;
import com.application.banque.produit.service.ProduitService;
import com.application.banque.utilitaires.Utilitaires;

@RunWith(SpringRunner.class)
@SpringBootTest
class UtilitairesTests {
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private AssocierService associerService;
	
	@Autowired
	private ProduitService produitService;
	
	@Autowired
	private CompteService compteService;

	@Test
	void clientExisteTest() throws ClientImplException {
		assertTrue(Utilitaires.clientExiste(1, clientService.afficherTousLesClients()));
		assertFalse(Utilitaires.clientExiste(56, clientService.afficherTousLesClients()));
		assertThrows(ClientImplException.class, () -> Utilitaires.clientExiste(1, null));
	}
	
	@Test
	void associationCompteProduitExisteTest() throws AssocierImplException {
		assertTrue(Utilitaires.associationCompteProduitExiste(1, 1, associerService.afficherLesAssociations()));
		assertFalse(Utilitaires.associationCompteProduitExiste(1, 10, associerService.afficherLesAssociations()));
		assertThrows(AssocierImplException.class, () -> Utilitaires.associationCompteProduitExiste(1, 10, null));
	}
	
	@Test
	void noCompteExisteTest() throws CompteImplException {
		assertTrue(Utilitaires.noCompteExiste(1, compteService.afficherTousLesComptes()));
		assertFalse(Utilitaires.noCompteExiste(56, compteService.afficherTousLesComptes()));
		assertThrows(CompteImplException.class, () -> Utilitaires.noCompteExiste(1, null));
	}
	
	@Test
	void codeProduitExisteTest() throws ProduitImplException {
		assertTrue(Utilitaires.codeProduitExiste(1, produitService.afficherTousLesProduits()));
		assertFalse(Utilitaires.codeProduitExiste(56, produitService.afficherTousLesProduits()));
		assertThrows(ProduitImplException.class, () -> Utilitaires.codeProduitExiste(2, null));
	}
	
	@Test
	void clientPossedeUnCompteTest() throws CompteImplException {
		assertTrue(Utilitaires.clientPossedeUnCompte(1, compteService.afficherTousLesComptes()));
		assertFalse(Utilitaires.clientPossedeUnCompte(56, compteService.afficherTousLesComptes()));
		assertThrows(CompteImplException.class, () -> Utilitaires.clientPossedeUnCompte(1, null));
	}
}
