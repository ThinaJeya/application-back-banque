package com.application.banque.tests;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.application.banque.associer.AssocierImplException;
import com.application.banque.associer.bean.Associer;
import com.application.banque.associer.service.AssocierService;
import com.application.banque.client.ClientImplException;
import com.application.banque.client.ClientImplException.MessageClientException;
import com.application.banque.client.bean.Client;
import com.application.banque.client.dao.ClientDaoService;
import com.application.banque.client.service.ClientServiceImpl;
import com.application.banque.compte.CompteImplException;
import com.application.banque.compte.CompteImplException.MessageCompteException;
import com.application.banque.compte.bean.Compte;
import com.application.banque.compte.dao.CompteDaoService;
import com.application.banque.compte.service.CompteService;
import com.application.banque.produit.ProduitImplException;
import com.application.banque.produit.bean.Produit;

/**
 * Classe de test de clientService
 * @author Pirathina Jeyakumar
 *
 */
@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class ClientServiceTest {

	@Mock
	private ClientDaoService clientDaoService;

	@Mock
	private CompteService compteService;

	@Mock
	private CompteDaoService compteDaoService;

	@Mock
	private AssocierService associerService;

	@InjectMocks
	private ClientServiceImpl clientServiceImpl;

	/**
	 * Liste des clients
	 */
	static List<Client> clients;

	/**
	 * Client
	 */
	static Optional<Client> client;

	/**
	 * Client créee sans code client
	 */
	static Client clientcree;

	/**
	 * Client créee avec un code client
	 */
	static Client clientCreeClassique;

	/**
	 * Liste de comptes
	 */
	static List<Compte> comptes;

	static List<Associer> associer;

	/**
	 * Méthode exécutée avant l'execution des tests
	 */
	@BeforeAll
	public static void before() {
		clients = new ArrayList<>();
		clients.add(new Client(1, "WinCh", "Pirio", new Date(1989-12-11), "45 Rue Henri Gautier", "44600", "Saint-Nazaire", "0306050904", null));
		clients.add(new Client(2, "JEyakuMar", "Largo", new Date(1994-05-11), "21 Jump Street", "69005", "Los Angeles", null, null));
		clients.add(new Client(3, "GriSSLo", "Rémi", new Date(1991-11-05), "56 Impasse des cerisiers", "03200", "La Gacilly", null, "remi_g@gmail.com"));

		client = Optional.of(new Client(1, "WinCh", "Pirio", new Date(1989-12-11), "45 Rue Henri Gautier", "44600", "Saint-Nazaire", "0306050904", null));

		clientcree = new Client("Doe", "John", new Date(1965-05-02), "10 rue de la paix", "75010", "Paris", null, "remi_g@gmail.com");

		clientCreeClassique = new Client(4, "Doe", "John", new Date(1965-05-02), "10 avenue de la Tour Eiffel", "75016", "Paris", "5556662221", "remi_g@gmail.com");

		comptes = new ArrayList<>();
		comptes.add(new Compte(new Date(2020-11-28), 3));
		comptes.add(new Compte(new Date(2020-11-15), 1));


		associer = new ArrayList<>();
		associer.add(new Associer(1, 1));
		associer.add(new Associer(1, 2));
		associer.add(new Associer(1, 4));
		associer.add(new Associer(2, 1));
		associer.add(new Associer(2, 5));
		associer.add(new Associer(2, 6));

	}

	/**
	 * Tests associés à la méthode afficherTousLesClients
	 * @throws ClientImplException 
	 */
	@Test
	void afficherTousLesClientsTest() throws ClientImplException{
		assertNotNull(afficherTousLesClientsTest_casClassique());

		Throwable listeClientNull = assertThrows(ClientImplException.class, () -> afficherTousLesClientsTest_listeRetourneeNull());
		assertEquals(listeClientNull.getMessage(), MessageClientException.LISTEOUPARAMETRENULLEOUVIDE.getMessage());
	}

	/**
	 * Test du cas où tout fonctionne
	 * @return liste des clients
	 * @throws ClientImplException
	 */
	private List<Client> afficherTousLesClientsTest_casClassique() throws ClientImplException {
		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(clients);
		} catch (Exception e) {
			fail("test raté pour afficherTousLesClientsTest_casClassique");
		}
		return this.clientServiceImpl.afficherTousLesClients();
	}

	/**
	 * Test du cas où la liste de clients retournée est null
	 * @return null
	 * @throws ClientImplException
	 */
	private List<Client> afficherTousLesClientsTest_listeRetourneeNull() throws ClientImplException {
		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(null);
		} catch (Exception e) {
			fail("test raté pour afficherTousLesClientsTest_listeRetourneeNull");
		}
		return this.clientServiceImpl.afficherTousLesClients();
	}


	/**
	 * Tests associés à la méthode afficherClientSelonCodeClient
	 * @throws ClientImplException client exception
	 */
	@Test
	void afficherClientSelonCodeClientTest() throws ClientImplException {
		assertNotNull(afficherClientSelonCodeClient_avecReponse());
		assertThrows(ClientImplException.class, () -> afficherClientSelonCodeClient_parametreZero());
	}

	/**
	 * Test du cas où le code client entré en paramètre vaut zéro
	 * @return client
	 * @throws ClientImplException
	 */
	private Client afficherClientSelonCodeClient_parametreZero() throws ClientImplException{
		int codeClient = 0;
		return this.clientServiceImpl.afficherClientSelonCodeClient(codeClient);
	}

	/**
	 * Test du cas classique d'affichage d'un client selon son code client
	 * @return client
	 * @throws ClientImplException
	 */
	private Client afficherClientSelonCodeClient_avecReponse() throws ClientImplException{
		int codeClient = 1;

		try {
			Mockito.when(clientDaoService.findById(codeClient)).thenReturn(client);
		} catch (Exception e) {
			fail("test raté pour afficherClientSelonCodeClient_avecReponse");
		}
		return this.clientServiceImpl.afficherClientSelonCodeClient(codeClient);
	}

	/**
	 *  Tests associés à la méthode afficherClientSelonNom
	 * @throws ClientImplException client exception
	 */
	@Test
	void afficherClientSelonNomTest() throws ClientImplException{
		assertNotNull(afficherClientSelonNom_avecReponse());
		assertEquals(1, afficherClientSelonNom_avecReponse().size());
		assertThrows(ClientImplException.class, () -> afficherClientSelonNom_ReponseNull());
		assertThrows(ClientImplException.class, () -> afficherClientSelonNom_parametreNull());
	}

	/**
	 * Test du cas classique d'affichage d'un client selon son nom
	 * @return client
	 * @throws ClientImplException
	 */
	private List<Client> afficherClientSelonNom_avecReponse() throws ClientImplException{
		String nom = "WiNCH";

		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(clients);
		} catch (Exception e) {
			fail("test raté pour afficherClientSelonNom_avecReponse");
		}
		return this.clientServiceImpl.afficherClientSelonNom(nom);
	}

	/**
	 * Test du cas où la réponse de la recherche de client(s) selon le nom entré en paramètre vaut null
	 * @return Liste de clients selon le nom entré en paramètre
	 * @throws ClientImplException
	 */
	private List<Client> afficherClientSelonNom_ReponseNull() throws ClientImplException{
		String nom = "DuPont";

		try {
			Mockito.when(clientServiceImpl.afficherTousLesClients()).thenReturn(null);
		} catch (Exception e) {
			fail("test raté pour afficherClientSelonNom_ReponseNull");
		}
		return this.clientServiceImpl.afficherClientSelonNom(nom);
	}

	/**
	 * Test du cas où le paramètre "nom" entré vaut null
	 * @return Liste de client(s)
	 * @throws ClientImplException
	 */
	private List<Client> afficherClientSelonNom_parametreNull() throws ClientImplException{
		String nom = null;

		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(clients);
		} catch (Exception e) {
			fail("test raté pour afficherClientSelonNom_parametreNull");
		}
		return this.clientServiceImpl.afficherClientSelonNom(nom);
	}

	/**
	 * Tests associés à la méthode creerClient
	 * @throws ClientImplException 
	 */
	@Test
	void creerClientTest() throws ClientImplException {

		Throwable clientVide = assertThrows(ClientImplException.class, () -> creerClient_clientNull());
		assertEquals(clientVide.getMessage(), MessageClientException.PARAMETREENTREINCORRECT.getMessage());

		Throwable nomObligatoireClientVide = assertThrows(ClientImplException.class, () -> creerClient_nomClientVide());
		assertEquals(nomObligatoireClientVide.getMessage(), MessageClientException.CHAMPCLIENTINCORRECT.getMessage());

		Throwable dateNaissanceObligatoireClientVide = assertThrows(ClientImplException.class, () -> creerClient_dateNaissanceClientVide());
		assertEquals(dateNaissanceObligatoireClientVide.getMessage(), MessageClientException.CHAMPCLIENTINCORRECT.getMessage());
	}

	/**
	 * Test dans le cas où le client à créer vaut null
	 * @throws ClientImplException
	 */
	private void creerClient_clientNull() throws ClientImplException {	
		clientServiceImpl.creerClient(null);
	}

	/**
	 * Test dans le cas où un des champs obligatoires (nom) de l'objet client à créer vaut null
	 * @throws ClientImplException
	 */
	private void creerClient_nomClientVide() throws ClientImplException {
		clientcree.setNom(null);
		clientServiceImpl.creerClient(clientcree);
	}

	/**
	 * Test dans le cas où un des champs obligatoires (date de naissance) de l'objet client à créer vaut null
	 * @throws ClientImplException
	 */
	private void creerClient_dateNaissanceClientVide() throws ClientImplException {
		clientcree.setDateNaissance(null);
		clientServiceImpl.creerClient(clientcree);
	}

	/**
	 * Tests associés à la méthode supprimerClient
	 */
	@Test
	void supprimerClientTest() {
		Throwable compteExistant = assertThrows(ClientImplException.class, () -> supprimerClient_compteExistant());
		assertEquals(compteExistant.getMessage(), MessageClientException.COMPTEEXISTANT.getMessage());

		Throwable codeClientVide = assertThrows(ClientImplException.class, () -> supprimerClient_codeClientZero());
		assertEquals(codeClientVide.getMessage(), MessageClientException.PARAMETREENTREINCORRECT.getMessage());
	}

	/**
	 * Test dans le cas où le code client entré en paramètre vaut 0
	 * @throws ClientImplException
	 * @throws CompteImplException
	 */
	private void supprimerClient_codeClientZero() throws ClientImplException, CompteImplException {
		clientServiceImpl.supprimerClient(0);
	}

	/**
	 * Test dans le cas où un client possédant un compte tente d'être supprimé
	 * @throws ClientImplException
	 * @throws CompteImplException
	 */
	private void supprimerClient_compteExistant() throws ClientImplException, CompteImplException {
		try {
			Mockito.when(clientDaoService.findById(ArgumentMatchers.anyInt())).thenReturn(client);
			Mockito.when(compteService.afficherComptesSelonCodeClient(ArgumentMatchers.anyInt())).thenReturn(comptes);
		} catch (Exception e) {
			fail("test raté pour supprimerClient_compteExistant : " + e);
		}
		clientServiceImpl.supprimerClient(2);
	}

	/**
	 * Tests associés à la méthode modifierTelephoneClient
	 * @throws ClientImplException
	 */
	@Test
	void modifierTelephoneClientTest() throws ClientImplException {
		Throwable clientPasTelephone = assertThrows(ClientImplException.class, () -> modifierTelephone_pasParametreObligatoire());
		assertEquals(clientPasTelephone.getMessage(), MessageClientException.PARAMETREENTREINCORRECT.getMessage());

		Throwable clientListeVide = assertThrows(ClientImplException.class, () -> modifierTelephone_listeClientTelephoneNull());
		assertEquals(clientListeVide.getMessage(), MessageClientException.LISTEOUPARAMETRENULLEOUVIDE.getMessage());

		Throwable clientPasTelephoneModifie = assertThrows(ClientImplException.class, () -> modifierTelephone_pasTelephoneModifie());
		assertEquals(clientPasTelephoneModifie.getMessage(), MessageClientException.CLIENTNAPASPUETREMODIFIE.getMessage());

		assertNotEquals(clients.get(0).getTelephone(), modifierTelephone_casClassique());
	}

	/**
	 * Test dans le cas où un des paramètres entrés est erroné
	 * @throws ClientImplException
	 */
	private void modifierTelephone_pasParametreObligatoire() throws ClientImplException {
		clientServiceImpl.modifierTelephoneClient(0, "0145210154");
	}

	/**
	 * Test dans le cas où la liste de clients sur lequel s'appuie la méthode est null
	 * @throws ClientImplException
	 */
	private void modifierTelephone_listeClientTelephoneNull() throws ClientImplException {
		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(null);
		} catch (Exception e) {
			fail();
		}
		clientServiceImpl.modifierTelephoneClient(2, "0145210154");
	}

	/**
	 * Test dans le cas où le téléphone n'a pas pu être modifié
	 * @throws ClientImplException
	 */
	private void modifierTelephone_pasTelephoneModifie() throws ClientImplException {
		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(clients);
		} catch (Exception e) {
			fail();
		}
		clientServiceImpl.miseAjourTelephoneClient(clients, 4, "0524154345", false);
	}

	/**
	 * Test du cas classique de modification du téléphone d'un client
	 * @return le téléphone modifié du client
	 * @throws ClientImplException
	 */
	private String modifierTelephone_casClassique() throws ClientImplException {
		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(clients);
		} catch (Exception e) {
			fail("test raté pour modifierTelephone_casClassique ; " + e);
		}

		clientServiceImpl.modifierTelephoneClient(1, "1000000001");

		return clients.get(0).getTelephone();
	}

	/**
	 * Tests associés à la méthode modifierEmailClient
	 * @throws ClientImplException
	 */
	@Test
	void modifierEmailClientTest() throws ClientImplException {
		Throwable clientListeNull = assertThrows(ClientImplException.class, () -> modifierEmailClientTest_listeClientNull());
		assertEquals(clientListeNull.getMessage(), MessageClientException.LISTEOUPARAMETRENULLEOUVIDE.getMessage());

		assertNotEquals(clients.get(1).getEmail(), modifierEmailClientTest_casClassique());
		Throwable emailPasModifie = assertThrows(ClientImplException.class, () ->modifierEmailClientTest_emailNonModifie());
		assertEquals(emailPasModifie.getMessage(), MessageClientException.CLIENTNAPASPUETREMODIFIE.getMessage());

		Throwable parametreIncorret = assertThrows(ClientImplException.class, () -> modifierEmailClientTest_ParametreIncorrect());
		assertEquals(parametreIncorret.getMessage(), MessageClientException.LISTEOUPARAMETRENULLEOUVIDE.getMessage());

		Throwable clientInexistant = assertThrows(ClientImplException.class, () -> modifierEmailClientTest_ClientInexistant());
		assertEquals(clientInexistant.getMessage(), MessageClientException.CLIENTNEXISTEPAS.getMessage());
	}

	/**
	 * Test dans le cas où la liste de clients sur lequel s'appuie la méthode est null
	 * @throws ClientImplException
	 */
	private void modifierEmailClientTest_listeClientNull() throws ClientImplException {
		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(null);
		} catch (Exception e) {
			fail("test raté pour modifierEmailClientTest_listeClientVide ; " + e);
		}
		clientServiceImpl.modifierEmailClient(2, "test.test@gmail.com");
	}

	/**
	 * Test du cas classique de modification de l'email d'un client
	 * @return l'email du client
	 * @throws ClientImplException
	 */
	private String modifierEmailClientTest_casClassique() throws ClientImplException {
		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(clients);
		} catch (Exception e) {
			fail("test raté pour modifierEmailClientTest_casClassique ; " + e);
		}
		clientServiceImpl.modifierEmailClient(2, "test.test@gmail.com");
		return clients.get(1).getEmail();
	}

	/**
	 * Test dans le cas où l'email n'a pas pu être modifié
	 * @throws ClientImplException
	 */
	private void modifierEmailClientTest_emailNonModifie() throws ClientImplException {
		clientServiceImpl.miseAjourEmailClient(clients, 4, "email.modifie@gmail.com", false);
	}

	/**
	 * Test dans le cas où l'un des paramètres entré est incorrect
	 * @throws ClientImplException
	 */
	private void modifierEmailClientTest_ParametreIncorrect() throws ClientImplException {
		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(clients);
		} catch (Exception e) {
			fail("test raté pour modifierEmailClientTest_ParametreIncorrect ; " + e);
		}
		clientServiceImpl.modifierEmailClient(0, "test.email-modifie@hotmail.com");
	}

	/**
	 * Test dans le cas où on cherche un client qui n'existe pas
	 * @throws ClientImplException
	 */
	private void modifierEmailClientTest_ClientInexistant() throws ClientImplException {
		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(clients);
		} catch (Exception e) {
			fail("test raté pour modifierEmailClientTest_ClientInexistant ; " + e);
		}
		clientServiceImpl.modifierEmailClient(10, "test.email-modifie@hotmail.com");
	}

	/**
	 * Tests associés à la méthode afficherProduitsSelonCodeClient
	 */
	@Test
	void afficherProduitsSelonCodeClientTest() {
		Throwable parametreIncorret = assertThrows(ClientImplException.class, () -> afficherProduitsSelonCodeClientTest_ParametreIncorrect());
		assertEquals(parametreIncorret.getMessage(), MessageClientException.PARAMETREENTREINCORRECT.getMessage());

		Throwable clientNexistePas = assertThrows(ClientImplException.class, () -> afficherProduitsSelonCodeClientTest_ClientInexistant());
		assertEquals(clientNexistePas.getMessage(), MessageClientException.CLIENTNEXISTEPAS.getMessage());

		Throwable compteNexistePas = assertThrows(CompteImplException.class, () -> afficherProduitsSelonCodeClientTest_CompteInexistant());
		assertEquals(compteNexistePas.getMessage(), MessageCompteException.PASDECOMPTE.getMessage());
	}

	/**
	 * Test dans le cas où le paramètre entré est incorrect
	 * @return liste de produits
	 * @throws ClientImplException
	 * @throws CompteImplException
	 * @throws AssocierImplException
	 * @throws ProduitImplException
	 */
	private List<Produit> afficherProduitsSelonCodeClientTest_ParametreIncorrect() throws ClientImplException, CompteImplException, AssocierImplException, ProduitImplException {
		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(clients);
		} catch (Exception e) {
			fail("test raté pour afficherProduitsSelonCodeClientTest_ParametreIncorrect ; " + e);
		}
		return clientServiceImpl.afficherProduitsSelonCodeClient(0);
	}

	/**
	 * Test dans le cas où le client recherché n'existe pas
	 * @return liste de produits
	 * @throws ClientImplException
	 * @throws CompteImplException
	 * @throws AssocierImplException
	 * @throws ProduitImplException
	 */
	private List<Produit> afficherProduitsSelonCodeClientTest_ClientInexistant() throws ClientImplException, CompteImplException, AssocierImplException, ProduitImplException {
		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(clients);
		} catch (Exception e) {
			fail("test raté pour afficherProduitsSelonCodeClientTest_ClientInexistant ; " + e);
		}
		return clientServiceImpl.afficherProduitsSelonCodeClient(10);
	}

	/**
	 * Test dans le cas où le(s) compte(s) recherché(s) n'existe(nt) pas
	 * @return liste de produits
	 * @throws CompteImplException
	 * @throws AssocierImplException
	 * @throws ClientImplException
	 * @throws ProduitImplException
	 */
	private List<Produit> afficherProduitsSelonCodeClientTest_CompteInexistant() throws CompteImplException, AssocierImplException, ClientImplException, ProduitImplException{
		try {
			Mockito.when(clientDaoService.findAll()).thenReturn(clients);
			Mockito.when(compteService.afficherTousLesComptes()).thenReturn(comptes);
		} catch (Exception e) {
			fail("test raté pour afficherProduitsSelonCodeClientTest_CompteInexistant ; " + e);
		}
		return clientServiceImpl.afficherProduitsSelonCodeClient(2);
	}
}
