package com.application.banque.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.application.banque.associer.service.AssocierService;
import com.application.banque.client.ClientImplException;
import com.application.banque.client.bean.Client;
import com.application.banque.client.service.ClientService;
import com.application.banque.compte.CompteImplException;
import com.application.banque.compte.CompteImplException.MessageCompteException;
import com.application.banque.compte.bean.Compte;
import com.application.banque.compte.dao.CompteDaoService;
import com.application.banque.compte.service.CompteService;
import com.application.banque.compte.service.CompteServiceimpl;
import com.application.banque.produit.ProduitImplException;

/**
 * Classe de test de compteService
 * @author Pirathina Jeyakumar
 *
 */
@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class CompteServiceTest {

	@Mock
	private ClientService clientService;

	@Mock
	private CompteService compteService;

	@Mock
	private CompteDaoService compteDaoService;

	@Mock
	private AssocierService associerService;

	@InjectMocks
	private CompteServiceimpl compteServiceimpl;

	/**
	 * Liste de comptes
	 */
	static List<Compte> comptes;

	static Optional<Compte> compteOptional;

	static Compte compte;

	static Compte compteSansClient;

	/**
	 * Client
	 */
	static Client client;

	/**
	 * Liste des clients
	 */
	static List<Client> clients;


	@BeforeAll
	public static void before() {

		comptes = new ArrayList<>();

		comptes.add(new Compte(1, new Date(2015-11-28), 3));
		comptes.add(new Compte(2, new Date(2015-12-15), 1));		
		comptes.add(new Compte(3, new Date(2016-05-11), 2));
		comptes.add(new Compte(4, new Date(2016-10-13), 3));
		comptes.add(new Compte(5, new Date(2017-06-07), 4));
		comptes.add(new Compte(6, new Date(2018-04-05), 5));

		compteOptional = Optional.of(new Compte(7, new Date(2018-01-20), 8));

		compte = new Compte(8, new Date(2019-07-06), 6);

		compteSansClient = new Compte(new Date(2019-07-06), 56);

		client = new Client(1, "WinCh", "Pirio", new Date(1989-12-11), "45 Rue Henri Gautier", "44600", "Saint-Nazaire", "0306050904", null);

		clients = new ArrayList<>();
		clients.add(new Client(1, "WinCh", "Pirio", new Date(1989-12-11), "45 Rue Henri Gautier", "44600", "Saint-Nazaire", "0306050904", null));
		clients.add(new Client(2, "JEyakuMar", "Largo", new Date(1994-05-11), "21 Jump Street", "69005", "Los Angeles", null, null));
		clients.add(new Client(3, "GriSSLo", "Rémi", new Date(1991-11-05), "56 Impasse des cerisiers", "03200", "La Gacilly", null, "remi_g@gmail.com"));
	}

	/**
	 * Tests associés à la méthode afficherTousLesComptes
	 */
	@Test
	void afficherTousLesComptesTest() {
		assertNotNull(afficherTousLesComptesTest_casClassique());		
	}

	private List<Compte> afficherTousLesComptesTest_casClassique(){	
		try {
			Mockito.when(compteDaoService.findAll()).thenReturn(comptes);			
		} catch (Exception e) {
			fail("test raté pour afficherTousLesComptesTest_casClassique " + e);
		}

		return this.compteServiceimpl.afficherTousLesComptes();
	}

	@Test
	void afficherCompteSelonNoCompteTest() throws CompteImplException {
		Throwable parametreIncorrect = assertThrows(CompteImplException.class, () -> afficherCompteSelonNoCompteTest_parametreIncorrect());
		assertEquals(parametreIncorrect.getMessage(), MessageCompteException.PARAMETREENTREINCORRECT.getMessage());

		Throwable pasDeCompte = assertThrows(CompteImplException.class, () -> afficherCompteSelonNoCompteTest_pasDeCompte());
		assertEquals(pasDeCompte.getMessage(), MessageCompteException.COMPTERETOURNENULL.getMessage());

		assertEquals(compteOptional.get().getNoCompte(), afficherCompteSelonNoCompteTest_casClassique());
	}

	private Compte afficherCompteSelonNoCompteTest_parametreIncorrect() throws CompteImplException {
		int noCompte = -5;		
		return this.compteServiceimpl.afficherCompteSelonNoCompte(noCompte);
	}

	private Compte afficherCompteSelonNoCompteTest_pasDeCompte() throws CompteImplException {
		int noCompte = 56;		
		return this.compteServiceimpl.afficherCompteSelonNoCompte(noCompte);
	}

	private int afficherCompteSelonNoCompteTest_casClassique() throws CompteImplException {
		int noCompte = 1;
		try {
			Mockito.when(compteDaoService.findById(ArgumentMatchers.anyInt())).thenReturn(compteOptional);
		} catch (Exception e) {
			fail("test raté pour afficherCompteSelonNoCompteTest_casClassique " + e);
		}
		return this.compteServiceimpl.afficherCompteSelonNoCompte(noCompte).getNoCompte();
	}

	@Test
	void afficherInfosClientCompteTest() throws ProduitImplException, ClientImplException, CompteImplException {

		Throwable parametreIncorrect = assertThrows(CompteImplException.class, () -> afficherInfosClientCompteTest_parametreIncorrect());
		assertEquals(parametreIncorrect.getMessage(), MessageCompteException.PARAMETREENTREINCORRECT.getMessage());

		Throwable compteRetourneNull = assertThrows(CompteImplException.class, () -> afficherInfosClientCompteTest_compteRetourneNull());
		assertEquals(compteRetourneNull.getMessage(), MessageCompteException.COMPTERETOURNENULL.getMessage());		
	}

	private Client afficherInfosClientCompteTest_parametreIncorrect() throws ProduitImplException, ClientImplException, CompteImplException {
		int noCompte = 0;
		return this.compteServiceimpl.afficherInfosClientCompte(noCompte);
	}

	private Client afficherInfosClientCompteTest_compteRetourneNull() throws ProduitImplException, ClientImplException, CompteImplException {		
		return this.compteServiceimpl.afficherInfosClientCompte(15);
	}

	@Test
	void creerCompteTest() {
		Throwable compteNull = assertThrows(CompteImplException.class, () -> creerCompteTest_erreurCreationObjet());
		assertEquals(compteNull.getMessage(), MessageCompteException.ERREURCREATIONOBJET.getMessage());
	}

	private void creerCompteTest_erreurCreationObjet() throws CompteImplException, ClientImplException {
		compteServiceimpl.creerCompte(null);
	}

	@Test
	void afficherComptesSelonCodeClientTest() {
		Throwable parametreIncorrect = assertThrows(CompteImplException.class, () -> afficherComptesSelonCodeClientTest_parametreIncorrect());
		assertEquals(parametreIncorrect.getMessage(), MessageCompteException.PARAMETREENTREINCORRECT.getMessage());

		Throwable listeComptesNull = assertThrows(CompteImplException.class, () -> afficherComptesSelonCodeClientTest_listeComptesNull());
		assertEquals(listeComptesNull.getMessage(), MessageCompteException.LISTENULLEOUVIDE.getMessage());
	}

	private List<Compte> afficherComptesSelonCodeClientTest_parametreIncorrect() throws CompteImplException {
		return compteServiceimpl.afficherComptesSelonCodeClient(0);
	}

	private List<Compte> afficherComptesSelonCodeClientTest_listeComptesNull() throws CompteImplException{
		Mockito.when(compteDaoService.findAll()).thenReturn(null);

		return this.compteServiceimpl.afficherComptesSelonCodeClient(2);
	}

}
