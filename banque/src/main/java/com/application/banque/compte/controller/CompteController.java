package com.application.banque.compte.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.application.banque.client.ClientImplException;
import com.application.banque.client.bean.Client;
import com.application.banque.compte.CompteImplException;
import com.application.banque.compte.bean.Compte;
import com.application.banque.compte.service.CompteService;
import com.application.banque.produit.ProduitImplException;

/**
 * Controller de Compte
 * @author Pirathina Jeyakumar
 *
 */
@RestController
@RequestMapping("/comptes")
public class CompteController {

	@Autowired
	private CompteService compteService;

	/**
	 * Controller pour afficher tous les comptes
	 * @return Liste de tous les comptes
	 */
	@GetMapping(path = "/tousLesComptes")
	public List<Compte> afficherTousLesComptes() {
		return compteService.afficherTousLesComptes();
	}

	/**
	 * Controller pour afficher le compte associé au noCompte entré en paramètre
	 * @param noCompte
	 * @return Compte au noCompte entré en paramètre
	 * @throws CompteImplException 
	 */
	@GetMapping(path = "/compte/{noCompte}")
	public Compte afficherCompteSelonNoCompte(@PathVariable int noCompte) throws CompteImplException {
		return compteService.afficherCompteSelonNoCompte(noCompte);
	}

	/**
	 * Controller pour afficher le client associé au noCompte entré en paramètre
	 * @param noCompte
	 * @return Client associé au noCompte entré en paramètre
	 * @throws ProduitImplException
	 * @throws ClientImplException
	 * @throws CompteImplException 
	 */
	@GetMapping(path = "/infosClientCompte/{noCompte}")
	public Client afficherInfosClientCompte(@PathVariable int noCompte) throws ProduitImplException, ClientImplException, CompteImplException {
		return compteService.afficherInfosClientCompte(noCompte);
	}

	/**
	 * Controller pour créer un nouveau compte
	 * @param compte
	 * @throws CompteImplException
	 * @throws ClientImplException
	 */
	@PostMapping(path = "/creerCompte")
	public void creerCompte(@RequestBody Compte compte) throws CompteImplException, ClientImplException {
		compteService.creerCompte(compte);
	}

	/**
	 * Controller pour afficher le(s) compte(s) associé(s) au codeClient entré en paramètre
	 * @param codeClient
	 * @return Compte(s) associé(s) au codeClient entré en paramètre
	 * @throws CompteImplException
	 */
	@GetMapping(path = "/comptesSelonCodeClient/{codeClient}")
	public List<Compte> afficherComptesSelonCodeClient(@PathVariable int codeClient) throws CompteImplException{
		return compteService.afficherComptesSelonCodeClient(codeClient);
	}
}
