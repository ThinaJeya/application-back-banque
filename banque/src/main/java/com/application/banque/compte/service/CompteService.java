package com.application.banque.compte.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.application.banque.client.ClientImplException;
import com.application.banque.client.bean.Client;
import com.application.banque.compte.CompteImplException;
import com.application.banque.compte.bean.Compte;
import com.application.banque.produit.ProduitImplException;

/**
 * Service de Compte
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface CompteService {

	/**
	 * Service pour afficher tous les comptes
	 * @return Liste de tous les comptes
	 */
	List<Compte> afficherTousLesComptes();

	/**
	 * Service pour afficher le compte associé au noCompte entré en paramètre
	 * @param noCompte
	 * @return Compte au noCompte entré en paramètre
	 * @throws CompteImplException 
	 */
	Compte afficherCompteSelonNoCompte(int noCompte) throws CompteImplException;

	/**
	 * Service pour afficher le client associé au noCompte entré en paramètre
	 * @param noCompte
	 * @return Client associé au noCompte entré en paramètre
	 * @throws ProduitImplException
	 * @throws ClientImplException
	 * @throws CompteImplException 
	 */
	Client afficherInfosClientCompte(int noCompte) throws ProduitImplException, ClientImplException, CompteImplException;

	/**
	 * Service pour créer un nouveau compte
	 * @param compte
	 * @throws CompteImplException
	 * @throws ClientImplException
	 */
	void creerCompte(Compte compte) throws CompteImplException, ClientImplException;

	/**
	 * Service pour afficher le(s) compte(s) associé(s) au codeClient entré en paramètre
	 * @param codeClient
	 * @return Compte(s) associé(s) au codeClient entré en paramètre
	 * @throws CompteImplException
	 */
	List<Compte> afficherComptesSelonCodeClient(int codeClient) throws CompteImplException;

}
