package com.application.banque.compte;

public class CompteImplException extends Exception{

	public enum MessageCompteException{

		LISTENULLEOUVIDE("La liste retournée est nulle ou vide"), 
		CHAMPCOMPTENULLEOUVIDE("Un des champs Compte est nulle ou vide"), 
		CLIENTNULL("Le code client entré n'existe pas dans la table Client"),
		COMPTERETOURNENULL("Le no_compte entré n'existe pas dans la table Compte"),
		PASDECOMPTE("Le client ne possède pas de compte"),
		PARAMETREENTREINCORRECT("Le paramètre entré est incorrect"),
		ERREURCREATIONOBJET("Un des paramètres de l'objet créée est incorrect");

		private String message;

		MessageCompteException(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}

	public CompteImplException(String messageException) {
		super(messageException);
	}
}
