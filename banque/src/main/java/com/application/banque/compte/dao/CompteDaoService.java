package com.application.banque.compte.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.application.banque.compte.bean.Compte;

/**
 * Interface Dao de Compte Service
 * @author Pirathina Jeyakumar
 *
 */
@Repository
public interface CompteDaoService extends JpaRepository<Compte, Integer>{

}
