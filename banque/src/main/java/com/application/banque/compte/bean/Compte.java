package com.application.banque.compte.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Couche Bean de Compte
 * @author Pirathina Jeyakumar
 *
 */
@Entity
@Table(name = "Compte")
public class Compte {

	/**
	 * Numéro de compte
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "no_compte")
	private int noCompte;

	/**
	 * Date de création du compte
	 */
	@Column(name = "date_creation")
	private Date dateCreation;

	/**
	 * Code client associé au compte
	 */
	@Column(name = "code_client")
	private int codeClient;

	public Compte() {
		super();
	}

	public Compte(Date dateCreation, int codeClient) {
		super();
		this.dateCreation = dateCreation;
		this.codeClient = codeClient;
	}
	
	public Compte(int noCompte, Date dateCreation, int codeClient) {
		super();
		this.noCompte = noCompte;
		this.dateCreation = dateCreation;
		this.codeClient = codeClient;
	}

	/**
	 * Permet d'obtenir le numéro de compte
	 * @return numéro de compte
	 */
	public int getNoCompte() {
		return noCompte;
	}

	/**
	 * Permet de modifier le numéro de compte
	 * @param noCompte
	 */
	public void setNoCompte(int noCompte) {
		this.noCompte = noCompte;
	}

	/**
	 * Permet d'obtenir la date de création du compte
	 * @return date de création du compte
	 */
	public Date getDateCreation() {
		return dateCreation;
	}

	/**
	 * Permet de modifier la date de création du compte
	 * @param dateCreation
	 */
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	/**
	 * Permet d'obtenir le code client associé au compte
	 * @return code client associé au compte
	 */
	public int getCodeClient() {
		return codeClient;
	}

	/**
	 * Permet de modifier le code client associé au compte
	 * @param codeClient
	 */
	public void setCodeClient(int codeClient) {
		this.codeClient = codeClient;
	}
}
