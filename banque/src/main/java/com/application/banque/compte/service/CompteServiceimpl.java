package com.application.banque.compte.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.application.banque.client.ClientImplException;
import com.application.banque.client.ClientImplException.MessageClientException;
import com.application.banque.client.bean.Client;
import com.application.banque.client.service.ClientService;
import com.application.banque.compte.CompteImplException;
import com.application.banque.compte.CompteImplException.MessageCompteException;
import com.application.banque.compte.bean.Compte;
import com.application.banque.compte.dao.CompteDaoService;
import com.application.banque.produit.ProduitImplException;
import com.application.banque.utilitaires.Utilitaires;

/**
 * Implémentation de Compte Service
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class CompteServiceimpl implements CompteService{

	@Autowired
	private CompteDaoService compteDaoService;

	@Autowired
	private ClientService clientService;

	@Override
	public List<Compte> afficherTousLesComptes() {
		return compteDaoService.findAll();
	}

	@Override
	public Compte afficherCompteSelonNoCompte(int noCompte) throws CompteImplException {
		Optional<Compte> resultat = null;

		if(noCompte > 0) {
			resultat = compteDaoService.findById(noCompte);
		} else {
			throw new CompteImplException(MessageCompteException.PARAMETREENTREINCORRECT.getMessage());
		}		
		return resultat.orElseThrow(() -> new CompteImplException(MessageCompteException.COMPTERETOURNENULL.getMessage()));
	}

	@Override
	public Client afficherInfosClientCompte(int noCompte) throws ProduitImplException, ClientImplException, CompteImplException {
		Client client = new Client();

		if(noCompte > 0) {
			Compte compte = afficherCompteSelonNoCompte(noCompte);

			if(!ObjectUtils.isEmpty(compte)) {			
				client = clientService.afficherClientSelonCodeClient(compte.getCodeClient());
				return client;
			} else {
				throw new ProduitImplException(MessageCompteException.COMPTERETOURNENULL.getMessage());
			}	
		} else {
			throw new CompteImplException(MessageCompteException.PARAMETREENTREINCORRECT.getMessage());
		}
	}

	@Override
	public void creerCompte(Compte compte) throws CompteImplException, ClientImplException {
		if(!ObjectUtils.isEmpty(compte) && compte.getDateCreation() != null && compte.getCodeClient() > 0) {
			boolean clientExiste = Utilitaires.clientExiste(compte.getCodeClient(), clientService.afficherTousLesClients());

			if(clientExiste) {
				compteDaoService.save(compte);
			} else {
				throw new ClientImplException(MessageClientException.CLIENTNEXISTEPAS.getMessage());
			}
		} else {
			throw new CompteImplException(MessageCompteException.ERREURCREATIONOBJET.getMessage());
		}
	}

	@Override
	public List<Compte> afficherComptesSelonCodeClient(int codeClient) throws CompteImplException {
		List<Compte> comptes = afficherTousLesComptes();
		List<Compte> resultats = new ArrayList<>();

		if(codeClient > 0) {
			if(!ObjectUtils.isEmpty(comptes)) {
				for(Compte compte: comptes) {
					if(compte.getCodeClient() == codeClient) {
						resultats.add(compte);
					}
				}
				return resultats;

			} else {
				throw new CompteImplException(MessageCompteException.LISTENULLEOUVIDE.getMessage());
			}	
		} else {
			throw new CompteImplException(MessageCompteException.PARAMETREENTREINCORRECT.getMessage());
		}
	}
}
