package com.application.banque.client.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.application.banque.associer.AssocierImplException;
import com.application.banque.associer.service.AssocierService;
import com.application.banque.client.ClientImplException;
import com.application.banque.client.ClientImplException.MessageClientException;
import com.application.banque.client.bean.Client;
import com.application.banque.client.dao.ClientDaoService;
import com.application.banque.compte.CompteImplException;
import com.application.banque.compte.CompteImplException.MessageCompteException;
import com.application.banque.compte.bean.Compte;
import com.application.banque.compte.service.CompteService;
import com.application.banque.produit.ProduitImplException;
import com.application.banque.produit.bean.Produit;
import com.application.banque.utilitaires.Utilitaires;

/**
 * Couche Implémentation de Service Client
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientDaoService clientDaoService;

	@Autowired
	private CompteService compteService;

	@Autowired
	private AssocierService associerService;

	@Override
	public List<Client> afficherTousLesClients() throws ClientImplException {
		List<Client> clients = clientDaoService.findAll();

		if(!ObjectUtils.isEmpty(clients)) {
			return clients;
		} else {
			throw new ClientImplException(MessageClientException.LISTEOUPARAMETRENULLEOUVIDE.getMessage());
		}
	}

	@Override
	public Client afficherClientSelonCodeClient(int codeClient) throws ClientImplException {
		Optional<Client> resultat = null;

		if(codeClient > 0) {
			resultat = clientDaoService.findById(codeClient);
		} else {
			throw new ClientImplException(MessageClientException.PARAMETREENTREINCORRECT.getMessage());
		}
		return resultat.orElseThrow(() -> new ClientImplException(MessageClientException.CLIENTNEXISTEPAS.getMessage()));
	}

	@Override
	public List<Client> afficherClientSelonNom(String nom) throws ClientImplException {
		List<Client> resultats = new ArrayList<>();
		List<Client> clients = afficherTousLesClients();

		if(!ObjectUtils.isEmpty(clients) && !StringUtils.isBlank(nom)) {
			for (Client client : clients) {
				if(client.getNom().toUpperCase().contains(nom.toUpperCase())) {
					Client clientReponse = client;
					resultats.add(clientReponse);
				}
			}
		} else {
			throw new ClientImplException(MessageClientException.LISTEOUPARAMETRENULLEOUVIDE.getMessage());
		}
		return resultats;
	}

	@Override
	public void creerClient(Client client) throws ClientImplException {

		if(!ObjectUtils.isEmpty(client)) {
			if(!StringUtils.isBlank(client.getNom()) 
					&& !StringUtils.isBlank(client.getPrenom()) 
					&& !ObjectUtils.isEmpty(client.getDateNaissance()) 
					&& !StringUtils.isBlank(client.getAdresse()) 
					&& !StringUtils.isBlank(client.getCodePostal()) 
					&& !StringUtils.isBlank(client.getVille())) {

				if(!StringUtils.isBlank(client.getEmail())) {
					String emailMisEnForme = Utilitaires.miseEnFormeEmail(client.getEmail());
					client.setEmail(emailMisEnForme);
					clientDaoService.save(client);
				} else {
					clientDaoService.save(client);
				}
			} else {
				throw new ClientImplException(MessageClientException.CHAMPCLIENTINCORRECT.getMessage());
			}
		} else {
			throw new ClientImplException(MessageClientException.PARAMETREENTREINCORRECT.getMessage());
		}
	}

	@Override
	public void supprimerClient(int codeClient) throws ClientImplException, CompteImplException {
		Client client = afficherClientSelonCodeClient(codeClient);

		if(!ObjectUtils.isEmpty(client) && codeClient > 0) {
			List<Compte> compte = compteService.afficherComptesSelonCodeClient(client.getCodeClient());

			if(ObjectUtils.isEmpty(compte)) {
				clientDaoService.deleteById(codeClient);				
			} else {
				throw new ClientImplException(MessageClientException.COMPTEEXISTANT.getMessage());
			}
		} else {
			throw new ClientImplException(MessageClientException.LISTEOUPARAMETRENULLEOUVIDE.getMessage());
		}
	}

	@Override
	public void modifierTelephoneClient(int codeClient, String telephone) throws ClientImplException {	
		boolean telephoneModifie = false;

		if(codeClient > 0 && !StringUtils.isBlank(telephone)) {
			List<Client> clients = afficherTousLesClients();

			if(Utilitaires.clientExiste(codeClient, clients)) {
				miseAjourTelephoneClient(clients, codeClient, telephone, telephoneModifie);
			} else {
				throw new ClientImplException(MessageClientException.CLIENTNEXISTEPAS.getMessage());
			}
		} else {
			throw new ClientImplException(MessageClientException.PARAMETREENTREINCORRECT.getMessage());
		}
	}

	/**
	 * Permet de mettre à jour le numéro de téléphone d'un client
	 * @param clients
	 * @param codeClient
	 * @param telephone
	 * @param telephoneModifie
	 * @throws ClientImplException
	 */
	public void miseAjourTelephoneClient(List<Client> clients, int codeClient, String telephone,
			boolean telephoneModifie) throws ClientImplException {
		if(!ObjectUtils.isEmpty(clients)) {
			for(Client client : clients) {
				if(!ObjectUtils.isEmpty(client) && client.getCodeClient() == codeClient) {
					client.setTelephone(telephone);
					clientDaoService.save(client);
					telephoneModifie = true;	
				}
			}
			if(!telephoneModifie) {
				throw new ClientImplException(MessageClientException.CLIENTNAPASPUETREMODIFIE.getMessage());
			}	
		}
	}

	@Override
	public void modifierEmailClient(int codeClient, String email) throws ClientImplException {
		boolean emailModifie = false;
		List<Client> clients = afficherTousLesClients();

		if(!ObjectUtils.isEmpty(clients) && codeClient > 0 && !StringUtils.isBlank(email) ) {
			if(Utilitaires.clientExiste(codeClient, clients)) {
				miseAjourEmailClient(clients, codeClient, email, emailModifie);
			} else {
				throw new ClientImplException(MessageClientException.CLIENTNEXISTEPAS.getMessage());
			}
		} else {
			throw new ClientImplException(MessageClientException.LISTEOUPARAMETRENULLEOUVIDE.getMessage());
		}
	}

	public void miseAjourEmailClient(List<Client> clients, int codeClient, String email, boolean emailModifie) throws ClientImplException {
		String emailMisEnForme = Utilitaires.miseEnFormeEmail(email);

		for(Client client: clients) {
			if(client.getCodeClient() == codeClient) {
				Client clientAModifier = client;
				clientAModifier.setEmail(emailMisEnForme);
				clientDaoService.save(clientAModifier);
				emailModifie = true;
			}
		}

		if(!emailModifie) {
			throw new ClientImplException(MessageClientException.CLIENTNAPASPUETREMODIFIE.getMessage());
		}
	}

	@Override
	public List<Produit> afficherProduitsSelonCodeClient(int codeClient) throws CompteImplException, AssocierImplException, ClientImplException, ProduitImplException {
		List<Produit> resultats = new ArrayList<>();

		if(codeClient > 0) {
			boolean clientExiste = Utilitaires.clientExiste(codeClient, afficherTousLesClients());

			if(clientExiste) {
				boolean clientPossedeUnCompte = Utilitaires.clientPossedeUnCompte(codeClient, compteService.afficherTousLesComptes());

				if(clientPossedeUnCompte) {
					List<Compte> comptesDuClient = compteService.afficherComptesSelonCodeClient(codeClient);

					for(Compte compte:comptesDuClient) {
						int noCompte = compte.getNoCompte();
						resultats.addAll(associerService.afficherLesDesignationsAssociationsSelonNoCompte(noCompte));
					}
				} else {
					throw new CompteImplException(MessageCompteException.PASDECOMPTE.getMessage());
				}
			} else {
				throw new ClientImplException(MessageClientException.CLIENTNEXISTEPAS.getMessage());
			}
		} else {
			throw new ClientImplException(MessageClientException.PARAMETREENTREINCORRECT.getMessage());
		}
		return resultats;
	}
}