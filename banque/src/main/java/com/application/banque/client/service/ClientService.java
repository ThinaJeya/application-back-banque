package com.application.banque.client.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.application.banque.associer.AssocierImplException;
import com.application.banque.client.ClientImplException;
import com.application.banque.client.bean.Client;
import com.application.banque.compte.CompteImplException;
import com.application.banque.produit.ProduitImplException;
import com.application.banque.produit.bean.Produit;

/**
 * Couche Service de Client
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface ClientService {

	/**
	 * Service pour afficher tous les clients
	 * @return Liste de tous les clients
	 * @throws ClientImplException 
	 */
	public List<Client> afficherTousLesClients() throws ClientImplException;

	/**
	 * Service pour afficher le client associé au code client entré en paramètre
	 * @param codeClient
	 * @return client associé au code client entré en paramètre
	 * @throws ClientImplException 
	 */
	public Client afficherClientSelonCodeClient(int codeClient) throws ClientImplException;

	/**
	 * Service pour afficher le(s) client(s) contenant le nom entré en paramètre
	 * @param nom
	 * @return Liste client(s) qui contiennent le nom entré en paramètre
	 * @throws ClientImplException
	 */
	public List<Client> afficherClientSelonNom(String nom) throws ClientImplException;

	/**
	 * Service pour créer un nouveau client
	 * @param client
	 * @throws ClientImplException
	 */
	public void creerClient(Client client) throws ClientImplException;

	/**
	 * Service pour supprimer le client associé au codeClient entré en paramètre
	 * @param codeClient
	 * @throws ClientImplException
	 * @throws CompteImplException 
	 */
	public void supprimerClient(int codeClient) throws ClientImplException, CompteImplException;

	/**
	 * Service pour mettre à jour le numéro de téléphone du client associé au codeClient entré
	 * @param codeClient
	 * @param telephone
	 * @throws ClientImplException 
	 */
	public void modifierTelephoneClient(int codeClient, String telephone) throws ClientImplException;

	/**
	 * Service pour mettre à jour l'email du client associé au codeClient entré
	 * @param codeClient
	 * @param email
	 * @throws ClientImplException 
	 */
	public void modifierEmailClient(int codeClient, String email) throws ClientImplException;

	/**
	 * Service pour afficher la liste des libellés de produits associés au codeClient entré en paramètre
	 * @param codeClient
	 * @return Liste des libellés de produits associés au client
	 * @throws CompteImplException
	 * @throws AssocierImplException
	 * @throws ClientImplException
	 * @throws ProduitImplException 
	 */
	public List<Produit> afficherProduitsSelonCodeClient(int codeClient) throws CompteImplException, AssocierImplException, ClientImplException, ProduitImplException;
}
