package com.application.banque.client.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Couche Bean du Client
 * @author Pirathina Jeyakumar
 *
 */
@Entity
@Table(name = "Client")
public class Client {

	/**
	 * code client du client
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Code_client")
	private int codeClient;

	/**
	 * Nom de famille du client
	 */
	@Column(name = "Nom")
	private String nom;

	/**
	 * Prénom du client
	 */
	@Column(name = "Prenom")
	private String prenom;

	/**
	 * Date de naissance du client (format : dd-MM-yyyy)
	 */
	@Column(name = "Date_Naissance")
	private Date dateNaissance;

	/**
	 * Numéro de rue et nom de rue du domicile
	 */
	@Column(name = "Adresse")
	private String adresse;

	/**
	 * Code postal de la ville de résidence
	 */
	@Column(name = "Code_Postal")
	private String codePostal;

	/**
	 * Ville de résidence
	 */
	@Column(name = "Ville")
	private String ville;

	/**
	 * Numéro de téléphone du client
	 */
	@Column(name = "Telephone")
	private String telephone;

	/**
	 * Adresse email du client
	 */
	@Column(name = "Email")
	private String email;

	public Client() {
	}

	public Client(String nom, String prenom, Date dateNaissance, String adresse, String codePostal,
			String ville, String telephone, String email) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.adresse = adresse;
		this.codePostal = codePostal;
		this.ville = ville;
		this.telephone = telephone;
		this.email = email;
	}
	
	public Client(int codeClient, String nom, String prenom, Date dateNaissance, String adresse, String codePostal,
			String ville, String telephone, String email) {
		super();
		this.codeClient = codeClient;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.adresse = adresse;
		this.codePostal = codePostal;
		this.ville = ville;
		this.telephone = telephone;
		this.email = email;
	}

	/**
	 * Permet d'obtenir le code client du client
	 * @return code client
	 */
	public int getCodeClient() {
		return codeClient;
	}

	/**
	 * Permet de modifier le code client du client
	 * @param codeClient
	 */
	public void setCodeClient(int codeClient) {
		this.codeClient = codeClient;
	}

	/**
	 * Permet d'obtenir le nom de famille du client
	 * @return nom de famille du client
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Permet de modifier le nom de famille du client
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Permet d'obtenir le prénom du client
	 * @return prénom du client
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Permet de modifier le prénom du client
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Permet d'obtenir date de naissance du client
	 * @return date de naissance du client
	 */
	public Date getDateNaissance() {
		return dateNaissance;
	}

	/**
	 * Permet de modifier la date de naissance du client
	 * @param dateNaissance
	 */
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	/**
	 * Permet d'obtenir le numéro de rue et nom de rue du domicile du client
	 * @return numéro de rue et nom de rue du domicile du client
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * Permet de modifier le numéro de rue et nom de rue du domicile du client
	 * @param adresse (numéro et nom de rue)
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/**
	 * Permet d'obtenir le code postal de la ville de résidence
	 * @return code postal de la ville de résidence
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * Permet de modifier le code postal de la ville de résidence
	 * @param codePostal
	 */
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * Permet d'obtenir la ville de résidence du client
	 * @return ville de résidence du client
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * Permet de modifier la ville de résidence du client
	 * @param ville
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * Permet d'obtenir le numéro de téléphone du client
	 * @return numéro de téléphone du client
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * Permet de modifier le numéro de téléphone du client
	 * @param telephone
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * Permet d'obtenir l'adresse email du client
	 * @return adresse email du client
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Permet de modifier l'adresse email du client
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Client [getCodeClient()=" + getCodeClient() + ", getNom()=" + getNom() + ", getPrenom()=" + getPrenom()
		+ ", getDateNaissance()=" + getDateNaissance() + ", getAdresse()=" + getAdresse() + ", getCodePostal()="
		+ getCodePostal() + ", getVille()=" + getVille() + ", getTelephone()=" + getTelephone()
		+ ", getEmail()=" + getEmail() + "]";
	}
}
