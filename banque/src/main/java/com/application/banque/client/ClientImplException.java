package com.application.banque.client;

public class ClientImplException extends Exception {

	public enum MessageClientException {
		PARAMETREENTREINCORRECT("Le paramètre entré est incorrect"),
		LISTEOUPARAMETRENULLEOUVIDE("Le paramètre entré est incorrect et/ou la liste de clients retournée : null ou vide"),
		CHAMPCLIENTINCORRECT("Un ou plusieurs champs client incorrect(s)"),
		COMPTEEXISTANT("Un ou plusieurs comptes existent pour ce client, les supprimer avant de supprimer le client"),
		CLIENTNEXISTEPAS("Le code client entré n'existe pas dans la table Client"),
		CLIENTNAPASPUETREMODIFIE("Le client n'a pas pu être modifié avec le paramètre entré");

		private String message;

		private MessageClientException(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}

	public ClientImplException(String messageException) {
		super(messageException);
	}
}
