package com.application.banque.client.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.application.banque.associer.AssocierImplException;
import com.application.banque.client.ClientImplException;
import com.application.banque.client.bean.Client;
import com.application.banque.client.service.ClientService;
import com.application.banque.compte.CompteImplException;
import com.application.banque.produit.ProduitImplException;
import com.application.banque.produit.bean.Produit;

/**
 * Controller de Client
 * @author Pirathina Jeyakumar
 *
 */
@RestController
@RequestMapping("/clients")
public class ClientController {

	@Autowired
	private ClientService clientService;

	/**
	 * Controller pour afficher tous les clients
	 * @return Liste de tous les clients
	 * @throws ClientImplException 
	 */
	@GetMapping(path = "/tousLesClients")
	public List<Client> afficherTousLesClients() throws ClientImplException{
		return clientService.afficherTousLesClients();
	}

	/**
	 * Controller pour afficher le client associé au code client entré en paramètre
	 * @param codeClient le code client associé
	 * @return client associé au code client entré en paramètre
	 * @throws ClientImplException 
	 */
	@GetMapping(path = "/client/{codeClient}")
	public Client afficherClientSelonCodeClient(@PathVariable int codeClient) throws ClientImplException {
		return clientService.afficherClientSelonCodeClient(codeClient);
	}

	/**
	 * Controller pour afficher le(s) client(s) contenant le nom entré en paramètre
	 * @param nom le nom du client
	 * @return le(s) client(s) contenant le nom entré en paramètre
	 * @throws ClientImplException
	 */
	@GetMapping(path = "/clientParNom/{nom}")
	public List<Client> afficherClientSelonNom(@PathVariable String nom) throws ClientImplException{
		return clientService.afficherClientSelonNom(nom);
	}

	/**
	 * Controller pour créer un nouveau client
	 * @param client le client
	 * @throws ClientImplException
	 */
	@PostMapping(path = "/creerNouveauClient")
	public void creerClient(@RequestBody Client client) throws ClientImplException {
		clientService.creerClient(client);
	}

	/**
	 * Controller pour supprimer le client associé au code client entré en paramètre
	 * @param codeClient le code client
	 * @throws ClientImplException
	 * @throws CompteImplException 
	 */
	@DeleteMapping(path = "/supprimerClient/{codeClient}")
	public void supprimerClient(@PathVariable int codeClient) throws ClientImplException, CompteImplException {
		clientService.supprimerClient(codeClient);
	}

	/**
	 * Controller pour modifier le numéro de téléphone du client associé au code client entré en paramètre
	 * @param codeClient le code client
	 * @param telephone le telephone
	 * @throws ClientImplException 
	 */
	@PostMapping(path = "/modifierTelephoneClient/{codeClient}")
	public void modifierTelephoneClient(@PathVariable int codeClient, @RequestBody String telephone) throws ClientImplException {
		clientService.modifierTelephoneClient(codeClient, telephone);
	}

	/**
	 * Controller pour modifier l'adresse email du client associé au code client entré en paramètre
	 * @param codeClient le code client
	 * @param email l'email
	 * @throws ClientImplException 
	 */
	@PostMapping(path = "/modifierEmailClient/{codeClient}")
	public void modifierEmailClient(@PathVariable int codeClient, @RequestBody String email) throws ClientImplException {
		clientService.modifierEmailClient(codeClient, email);
	}

	/**
	 * Controller pour afficher la liste des produits associés au client dont le numéro a été entré en paramètre
	 * @param codeClient le code client
	 * @return la liste des produits associés au client
	 * @throws CompteImplException
	 * @throws AssocierImplException
	 * @throws ClientImplException
	 * @throws ProduitImplException 
	 */
	@GetMapping(path = "/produitsSelonCodeClient/{codeClient}")
	public List<Produit> afficherProduitsSelonCodeClient(@PathVariable int codeClient) throws CompteImplException, AssocierImplException, ClientImplException, ProduitImplException{
		return clientService.afficherProduitsSelonCodeClient(codeClient);
	}
}
