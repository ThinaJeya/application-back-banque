package com.application.banque.client.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.application.banque.client.bean.Client;

/**
 * Interface Dao de Client Service
 * @author Pirathina Jeyakumar
 *
 */
@Repository
public interface ClientDaoService extends JpaRepository<Client, Integer> {

}
