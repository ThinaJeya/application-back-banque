package com.application.banque.utilitaires;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;

import com.application.banque.associer.AssocierImplException;
import com.application.banque.associer.AssocierImplException.MessageAssocierException;
import com.application.banque.associer.bean.Associer;
import com.application.banque.client.ClientImplException;
import com.application.banque.client.ClientImplException.MessageClientException;
import com.application.banque.client.bean.Client;
import com.application.banque.compte.CompteImplException;
import com.application.banque.compte.CompteImplException.MessageCompteException;
import com.application.banque.compte.bean.Compte;
import com.application.banque.produit.ProduitImplException;
import com.application.banque.produit.ProduitImplException.MessageProduitException;
import com.application.banque.produit.bean.Produit;

/**
 * Méthodes utilitaires
 * @author Pirathina Jeyakumar
 *
 */
public class Utilitaires {

	private Utilitaires() {
	}

	/**
	 * Vérifie que le client associé au codeClient entré en paramètre existe
	 * @param codeClient le code client
	 * @param clients les clients
	 * @return vrai si le client existe
	 * @throws ClientImplException
	 */
	public static boolean clientExiste(int codeClient, List<Client> clients) throws ClientImplException {
		boolean reponse = false;

		if(!ObjectUtils.isEmpty(clients) && codeClient > 0) {
			reponse = clients.stream().anyMatch(client -> client.getCodeClient() == codeClient);
		} else {
			throw new ClientImplException(MessageClientException.LISTEOUPARAMETRENULLEOUVIDE.getMessage());
		}
		return reponse;
	}

	/**
	 * Vérifie que l'association entre le noCompte et le codeProduit entrés en paramètres existe
	 * @param noCompte
	 * @param codeProduit
	 * @param associers
	 * @return vrai si l'association existe
	 * @throws AssocierImplException
	 */
	public static boolean associationCompteProduitExiste(int noCompte, int codeProduit, List<Associer> associers) throws AssocierImplException{
		boolean reponse = false;

		if(!ObjectUtils.isEmpty(associers)) {			
			reponse = associers.stream().anyMatch(associer -> associer.getCodeProduit() == codeProduit && associer.getNoCompte() == noCompte);
		} else {
			throw new AssocierImplException(MessageAssocierException.LISTENULLEOUVIDE.getMessage());
		}
		return reponse;
	}

	/**
	 * Vérifie que le compte associé au noCompte entré en paramètre existe
	 * @param noCompte
	 * @param comptes
	 * @return vrai si le compte existe
	 * @throws CompteImplException
	 */
	public static boolean noCompteExiste(int noCompte, List<Compte> comptes) throws CompteImplException {
		boolean reponse = false;		

		if(!ObjectUtils.isEmpty(comptes)) {
			reponse = comptes.stream().anyMatch(compte -> compte.getNoCompte() == noCompte);		
		} else {
			throw new CompteImplException(MessageCompteException.LISTENULLEOUVIDE.getMessage());
		}
		return reponse;
	}

	/**
	 * Vérifie que le produit associé au codeProduit entré en paramètre existe
	 * @param codeProduit
	 * @param produits
	 * @return vrai si le produit existe
	 * @throws ProduitImplException
	 */
	public static boolean codeProduitExiste(int codeProduit, List<Produit> produits) throws ProduitImplException {
		boolean reponse = false;

		if(!ObjectUtils.isEmpty(produits)) {
			reponse = produits.stream().anyMatch(produit -> produit.getCodeProduit() == codeProduit);
		} else {
			throw new ProduitImplException(MessageProduitException.LISTENULLEOUVIDE.getMessage());
		}
		return reponse;
	}

	/**
	 * Vérifie que le client associé au codeClient entré en paramètre possède un ou des compte(s)
	 * @param codeClient
	 * @param comptes
	 * @return vrai si le client possède au moins un compte
	 * @throws CompteImplException
	 */
	public static boolean clientPossedeUnCompte(int codeClient, List<Compte> comptes) throws CompteImplException {
		boolean reponse = false;

		if(!ObjectUtils.isEmpty(comptes) && codeClient > 0) {
			reponse = comptes.stream().anyMatch(compte -> compte.getCodeClient() == codeClient);
		} else {
			throw new CompteImplException(MessageCompteException.LISTENULLEOUVIDE.getMessage());
		}
		return reponse;
	}
	
	/**
	 * Met en forme l'email avant son enregistrement
	 * @param email
	 * @return email mis en forme
	 */
	public static String miseEnFormeEmail(String email) {
		String[] emailResultats;
		String emailUpperCase = "";
		StringBuilder emailBuilder = new StringBuilder();
		if(!StringUtils.isBlank(email) && email.contains(".")){
			emailResultats = email.split("\\.");

			for(String emailResultat : emailResultats) {
				emailResultat = emailResultat.substring(0, 1).toUpperCase() + emailResultat.substring(1);
				emailResultat += ".";
				emailBuilder.append(emailResultat);
			}
			emailUpperCase = emailBuilder.toString();

		} else {
			emailUpperCase = email.substring(0, 1).toUpperCase() + email.substring(1);
		}

		boolean seTermineParPoint = emailUpperCase.endsWith(".");

		if(seTermineParPoint) {
			emailUpperCase = emailUpperCase.substring(0, emailUpperCase.length()-1);
		}

		return emailUpperCase;
	}
}
