package com.application.banque.associer.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.application.banque.associer.AssocierImplException;
import com.application.banque.associer.AssocierImplException.MessageAssocierException;
import com.application.banque.associer.bean.Associer;
import com.application.banque.associer.dao.AssocierServiceDao;
import com.application.banque.compte.CompteImplException;
import com.application.banque.compte.service.CompteService;
import com.application.banque.produit.ProduitImplException;
import com.application.banque.produit.bean.Produit;
import com.application.banque.produit.service.ProduitService;
import com.application.banque.utilitaires.Utilitaires;

/**
 * Implémentation d'Associer Service
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class AssocierServiceImpl implements AssocierService {

	@Autowired
	private AssocierServiceDao associerServiceDao;

	@Autowired
	private CompteService compteService;

	@Autowired
	private ProduitService produitService;

	@Override
	public List<Associer> afficherLesAssociations() {
		return associerServiceDao.findAll();
	}

	@Override
	public List<Associer> afficherLesAssociationsSelonNoCompte(int noCompte) throws AssocierImplException, CompteImplException {
		List<Associer> associations = afficherLesAssociations();
		List<Associer> resultats = new ArrayList<>();

		if(noCompte > 0 && Utilitaires.noCompteExiste(noCompte, compteService.afficherTousLesComptes())) {
			if(!ObjectUtils.isEmpty(associations)) {
				for(Associer association:associations) {
					if(association.getNoCompte() == noCompte) {
						resultats.add(association);
					}
				}
			} else {
				throw new AssocierImplException(MessageAssocierException.LISTENULLEOUVIDE.getMessage());
			}
		} else {
			throw new AssocierImplException(MessageAssocierException.PARAMETREENTREINCORRECT.getMessage());
		}
		return resultats;
	}

	@Override
	public List<Produit> afficherLesDesignationsAssociationsSelonNoCompte(int noCompte) throws AssocierImplException, CompteImplException, ProduitImplException {
		List<Associer> associations = afficherLesAssociationsSelonNoCompte(noCompte);
		List<Produit> resultats = new ArrayList<>();

		if(noCompte > 0) {
			if(!ObjectUtils.isEmpty(associations)) {			
				for(Associer association: associations) {
					Produit produit = produitService.afficherProduitSelonCodeProduit(association.getCodeProduit());
					resultats.add(produit);
				}
			} else {
				throw new AssocierImplException(MessageAssocierException.LISTENULLEOUVIDE.getMessage());
			}
		} else {
			throw new AssocierImplException(MessageAssocierException.PARAMETREENTREINCORRECT.getMessage());
		}

		return resultats;
	}

	@Override
	public void creerAssociation(Associer associer) throws AssocierImplException, CompteImplException, ProduitImplException {		

		if(associer.getCodeProduit() > 0 && associer.getNoCompte() > 0) {
			boolean associationExiste = Utilitaires.associationCompteProduitExiste(associer.getNoCompte(), associer.getCodeProduit(), afficherLesAssociations());

			if(!associationExiste) {
				boolean noCompteExiste = Utilitaires.noCompteExiste(associer.getNoCompte(), compteService.afficherTousLesComptes());
				boolean codeProduitExiste = Utilitaires.codeProduitExiste(associer.getCodeProduit(), produitService.afficherTousLesProduits());

				if(noCompteExiste && codeProduitExiste) {
					associerServiceDao.save(associer);
				} else {
					throw new AssocierImplException(MessageAssocierException.ERREURCHAMPASSOCIER.getMessage());
				}	
			} else {
				throw new AssocierImplException(MessageAssocierException.ASSOCIATIONEXISTEDEJA.getMessage());
			}	
		} else {
			throw new AssocierImplException(MessageAssocierException.ERREURCREATIONOBJET.getMessage());
		}
	}

	@Override
	public void supprimerAssociation(Associer associer) throws AssocierImplException {

		if(associer.getCodeProduit() > 0 && associer.getNoCompte() > 0) {
			boolean associationExiste = Utilitaires.associationCompteProduitExiste(associer.getNoCompte(), associer.getCodeProduit(), afficherLesAssociations());

			if(associationExiste) {
				associerServiceDao.delete(associer);			
			} else {
				throw new AssocierImplException(MessageAssocierException.ASSOCIATIONNEXISTEPAS.getMessage());
			}
		} else {
			throw new AssocierImplException(MessageAssocierException.ERREURCREATIONOBJET.getMessage());
		}
	}
}
