package com.application.banque.associer.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.application.banque.associer.AssocierImplException;
import com.application.banque.associer.bean.Associer;
import com.application.banque.compte.CompteImplException;
import com.application.banque.produit.ProduitImplException;
import com.application.banque.produit.bean.Produit;

/**
 * Service d'Associer
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface AssocierService {

	/**
	 * Service pour afficher toutes les associations entre codes produits et numéros de comptes
	 * @return Liste de toutes les associations produits/comptes
	 */
	List<Associer> afficherLesAssociations();

	/**
	 * Service pour afficher toutes les associations entre les codes produits et le noCompte entré en paramètre
	 * @param noCompte
	 * @return Liste des associations entre produits et le noCompte entré en paramètre
	 * @throws AssocierImplException 
	 * @throws CompteImplException 
	 */
	List<Associer> afficherLesAssociationsSelonNoCompte(int noCompte) throws AssocierImplException, CompteImplException;

	/**
	 * Service pour afficher les désignations des codes produits associés au noCompte entré en paramètre
	 * @param noCompte
	 * @return Liste des désignations des produits associés au noCompte entré en paramètre
	 * @throws AssocierImplException
	 * @throws CompteImplException 
	 * @throws ProduitImplException 
	 */
	List<Produit> afficherLesDesignationsAssociationsSelonNoCompte(int noCompte) throws AssocierImplException, CompteImplException, ProduitImplException;

	/**
	 * Service pour créer une nouvelle association entre un codeProduit et noCompte
	 * @param associer
	 * @throws AssocierImplException
	 * @throws CompteImplException
	 * @throws ProduitImplException
	 */
	void creerAssociation(Associer associer) throws AssocierImplException, CompteImplException, ProduitImplException;

	/**
	 * Service pour supprimer une association
	 * @param associer
	 * @throws AssocierImplException
	 */
	void supprimerAssociation(Associer associer) throws AssocierImplException;

}
