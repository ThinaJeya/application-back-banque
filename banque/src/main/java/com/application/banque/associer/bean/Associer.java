package com.application.banque.associer.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Couche Bean d'Associer
 * @author Pirathina Jeyakumar
 *
 */
@Entity
@Table(name = "Associer")
@IdClass(Associer.class)
public class Associer implements Serializable{

	/**
	 * Numéro de compte
	 */
	@Id
	@Column(name = "no_compte")
	private int noCompte;

	/**
	 * Code produit
	 */
	@Id
	@Column(name = "code_produit")
	private int codeProduit;

	public Associer() {
		super();
	}

	public Associer(int noCompte, int codeProduit) {
		super();
		this.noCompte = noCompte;
		this.codeProduit = codeProduit;
	}

	/**
	 * Permet d'obtenir le numéro de compte
	 * @return numéro de compte
	 */
	public int getNoCompte() {
		return noCompte;
	}

	/**
	 * Permet de modifier le numéro de compte
	 * @param noCompte
	 */
	public void setNoCompte(int noCompte) {
		this.noCompte = noCompte;
	}

	/**
	 * Permet d'obtenir le code produit
	 * @return code produit
	 */
	public int getCodeProduit() {
		return codeProduit;
	}

	/**
	 * Permet de modifier le code produit
	 * @param codeProduit
	 */
	public void setCodeProduit(int codeProduit) {
		this.codeProduit = codeProduit;
	}	
}
