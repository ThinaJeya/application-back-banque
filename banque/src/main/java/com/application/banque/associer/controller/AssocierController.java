package com.application.banque.associer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.application.banque.associer.AssocierImplException;
import com.application.banque.associer.bean.Associer;
import com.application.banque.associer.service.AssocierService;
import com.application.banque.compte.CompteImplException;
import com.application.banque.produit.ProduitImplException;
import com.application.banque.produit.bean.Produit;

/**
 * Controller d'Associer
 * @author Pirathina Jeyakumar
 *
 */
@RestController
@RequestMapping("/associer")
public class AssocierController {

	@Autowired
	private AssocierService associerService;

	/**
	 * Controller pour afficher toutes les associations entre codes produits et numéros de comptes
	 * @return Liste de toutes les associations produits/comptes
	 */
	@GetMapping(path = "/associations")
	public List<Associer> afficherLesAssociations() {
		return associerService.afficherLesAssociations();
	}

	/**
	 * Controller pour afficher toutes les associations entre les codes produits et le noCompte entré en paramètre
	 * @param noCompte
	 * @return Liste des associations entre produits et le noCompte entré en paramètre
	 * @throws AssocierImplException 
	 * @throws CompteImplException 
	 */
	@GetMapping(path = "/associationsSelonNoCompte/{noCompte}")
	public List<Associer> afficherLesAssociationsSelonNoCompte(@PathVariable int noCompte) throws AssocierImplException, CompteImplException{
		return associerService.afficherLesAssociationsSelonNoCompte(noCompte);
	}

	/**
	 * Controller pour afficher les désignations des codes produits associés au noCompte entré en paramètre
	 * @param noCompte
	 * @return Liste des désignations des produits associés au noCompte entré en paramètre
	 * @throws AssocierImplException
	 * @throws CompteImplException 
	 * @throws ProduitImplException 
	 */
	@GetMapping(path = "/designationsAssociationsParCompte/{noCompte}")
	public List<Produit> afficherLesDesignationsAssociationsSelonNoCompte(@PathVariable int noCompte) throws AssocierImplException, CompteImplException, ProduitImplException{
		return associerService.afficherLesDesignationsAssociationsSelonNoCompte(noCompte);
	}

	/**
	 * Controller pour créer une nouvelle association entre un codeProduit et noCompte
	 * @param associer
	 * @throws AssocierImplException
	 * @throws CompteImplException
	 * @throws ProduitImplException
	 */
	@PostMapping(path = "/creerAssociation")
	public void creerAssociation(@RequestBody Associer associer) throws AssocierImplException, CompteImplException, ProduitImplException {
		associerService.creerAssociation(associer);
	}

	/**
	 * Controller pour supprimer une association
	 * @param associer
	 * @throws AssocierImplException
	 */
	@DeleteMapping(path = "/supprimerAssociation")
	public void supprimerAssociationSelonCodeProduit(@RequestBody Associer associer) throws AssocierImplException {
		associerService.supprimerAssociation(associer);
	}
}
