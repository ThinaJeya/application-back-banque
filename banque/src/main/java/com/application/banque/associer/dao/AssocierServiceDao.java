package com.application.banque.associer.dao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.application.banque.associer.bean.Associer;

/**
 * Interface Dao d'Associer Service
 * @author Pirathina Jeyakumar
 *
 */
@Qualifier("Associer")
@Repository
public interface AssocierServiceDao extends JpaRepository<Associer, Integer>{

}
