package com.application.banque.associer;

public class AssocierImplException extends Exception{

	public enum MessageAssocierException{
		LISTENULLEOUVIDE("Liste retournée : nulle ou vide"),
		PASDECORRESPONDANCE("Aucune correspondance trouvée"),
		ERREURCHAMPASSOCIER("Une des champs de l'objet n'existe pas (no_compte et/ou code_produit"),
		ASSOCIATIONEXISTEDEJA("L'association existe déjà"),
		ASSOCIATIONNEXISTEPAS("L'association n'existe pas"),
		PARAMETREENTREINCORRECT("Le paramètre entré est incorrect ou n'existe pas"),
		ERREURCREATIONOBJET("Un des paramètres de l'objet créée est incorrect");

		private String message;

		private MessageAssocierException(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}

	public AssocierImplException(String messageException) {
		super(messageException);
	}
}
