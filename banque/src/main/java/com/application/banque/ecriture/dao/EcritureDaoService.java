package com.application.banque.ecriture.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.application.banque.ecriture.bean.Ecriture;

/**
 * Interface Dao d'Ecriture Service
 * @author Pirathina Jeyakumar
 *
 */
@Repository
public interface EcritureDaoService extends JpaRepository<Ecriture, Integer>{

}
