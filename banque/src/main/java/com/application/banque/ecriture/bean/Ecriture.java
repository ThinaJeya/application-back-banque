package com.application.banque.ecriture.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Couche Bean d'Ecriture
 * @author Pirathina Jeyakumar
 *
 */
@Entity
@Table(name = "Ecriture")
public class Ecriture {

	/**
	 * Numéro d'écriture
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "no_ecriture")
	private int noEcriture;

	/**
	 * Date de l'écriture
	 */
	@Column(name = "date_ecriture")
	private Date dateEcriture;

	/**
	 * Valeur débit de l'écriture
	 */
	@Column(name = "debit")
	private Float debit;

	/**
	 * Valeur crédit de l'écriture
	 */
	@Column(name = "credit")
	private Float credit;

	/**
	 * Numéro de compte associé à l'écriture
	 */
	@Column(name = "no_compte")
	private int noCompte;

	public Ecriture() {
		super();
	}

	public Ecriture(Date dateEcriture, Float debit, Float credit, int noCompte) {
		super();
		this.dateEcriture = dateEcriture;
		this.debit = debit;
		this.credit = credit;
		this.noCompte = noCompte;
	}


	public Ecriture(int noEcriture, Date dateEcriture, Float debit, Float credit, int noCompte) {
		super();
		this.noEcriture = noEcriture;
		this.dateEcriture = dateEcriture;
		this.debit = debit;
		this.credit = credit;
		this.noCompte = noCompte;
	}

	/**
	 * Permet d'obtenir le numéro d'écriture
	 * @return numéro d'écriture
	 */
	public int getNoEcriture() {
		return noEcriture;
	}

	/**
	 * Permet de modifier le numéro d'écriture
	 * @param noEcriture
	 */
	public void setNoEcriture(int noEcriture) {
		this.noEcriture = noEcriture;
	}

	/**
	 * Permet d'obtenir la date d'écriture
	 * @return date d'écriture
	 */
	public Date getDateEcriture() {
		return dateEcriture;
	}

	/**
	 * Permet de modifier la date d'écriture
	 * @param dateEcriture
	 */
	public void setDateEcriture(Date dateEcriture) {
		this.dateEcriture = dateEcriture;
	}

	/**
	 * Permet d'obtenir la valeur débit de l'écriture
	 * @return valeur débit de l'écriture
	 */
	public Float getDebit() {
		return debit;
	}

	/**
	 * Permet de modifier la valeur débit de l'écriture
	 * @param debit
	 */
	public void setDebit(Float debit) {
		this.debit = debit;
	}

	/**
	 * Permet d'obtenir la valeur crédit de l'écriture
	 * @return valeur crédit de l'écriture
	 */
	public Float getCredit() {
		return credit;
	}

	/**
	 * Permet de modifier la valeur crédit de l'écriture
	 * @param credit
	 */
	public void setCredit(Float credit) {
		this.credit = credit;
	}

	/**
	 * Permet d'obtenir le numéro de compte associé à l'écriture
	 * @return numéro de compte associé à l'écriture
	 */
	public int getNoCompte() {
		return noCompte;
	}

	/**
	 * Permet de modifier le numéro de compte associé à l'écriture
	 * @param noCompte
	 */
	public void setNoCompte(int noCompte) {
		this.noCompte = noCompte;
	}

	@Override
	public String toString() {
		return "Ecriture [getNoEcriture()=" + getNoEcriture() + ", getDateEcriture()=" + getDateEcriture()
		+ ", getDebit()=" + getDebit() + ", getCredit()=" + getCredit() + ", getNoCompte()=" + getNoCompte()
		+ "]";
	}

}
