package com.application.banque.ecriture.controller;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.application.banque.compte.CompteImplException;
import com.application.banque.ecriture.EcritureImplException;
import com.application.banque.ecriture.bean.Ecriture;
import com.application.banque.ecriture.service.EcritureService;

/**
 * Controller d'Ecriture
 * @author Pirathina Jeyakumar
 *
 */
@RestController
@RequestMapping("/ecritures")
public class EcritureController {

	@Autowired
	private EcritureService ecritureService;

	/**
	 * Controller pour afficher toutes les écritures
	 * @return Liste de toutes les écritures
	 */
	@GetMapping(path = "/toutesLesEcritures")
	public List<Ecriture> afficherToutesLesEcritures(){
		return ecritureService.afficherToutesLesEcritures();
	}

	/**
	 * Controller pour afficher les écritures associées au noCompte entré en paramètre
	 * @param noCompte
	 * @return Liste des écritures associées au noCompte entré en paramètre
	 * @throws EcritureImplException
	 * @throws CompteImplException
	 */
	@GetMapping(path = "/ecrituresDuCompte/{noCompte}")
	public List<Ecriture> afficherEcrituresCompte(@PathVariable int noCompte) throws EcritureImplException, CompteImplException{
		return ecritureService.afficherEcrituresCompte(noCompte);
	}

	/**
	 * Controller pour afficher les écritures enregistrées à la date entrée en paramètre
	 * @param dateEcriture
	 * @return Liste des écritures enregistrées à la date entrée en paramètre
	 * @throws EcritureImplException
	 */
	@GetMapping(path = "/ecrituresSelonDate/{dateEcriture}")
	public List<Ecriture> afficherEcrituresSelonDate(@PathVariable Date dateEcriture) throws EcritureImplException{
		return ecritureService.afficherEcrituresSelonDate(dateEcriture);
	}

	/**
	 * Controller pour afficher les écritures enregistrées entre les deux dates entrées en paramètre
	 * @param dateEcritureDebut
	 * @param dateEcritureFin
	 * @return Liste des écritures enregistrées entre les deux dates entrées en paramètre
	 * @throws EcritureImplException 
	 */
	@GetMapping(path = "/ecrituresEntreDates/{dateEcritureDebut}/{dateEcritureFin}")
	public List<Ecriture> afficherEcrituresEntreDeuxDates(@PathVariable Date dateEcritureDebut, @PathVariable Date dateEcritureFin) throws EcritureImplException{
		return ecritureService.afficherEcrituresEntreDeuxDates(dateEcritureDebut, dateEcritureFin);
	}

	/**
	 * Controller pour afficher le solde du compte associé au noCompte entré en paramètre
	 * @param noCompte
	 * @return solde du compte associé au noCompte entré en paramètre
	 * @throws EcritureImplException
	 * @throws CompteImplException
	 */
	@GetMapping(path = "/soldeCompte/{noCompte}")
	public String afficherSoldeCompte(@PathVariable int noCompte) throws EcritureImplException, CompteImplException {
		return ecritureService.afficherSoldeCompte(noCompte);
	}

	/**
	 * Controller pour créer une nouvelle écriture
	 * @param ecriture
	 * @throws EcritureImplException
	 * @throws CompteImplException
	 */
	@PostMapping(path = "/creerEcriture")
	public void creerEcriture(@RequestBody Ecriture ecriture) throws EcritureImplException, CompteImplException {
		ecritureService.creerEcriture(ecriture);
	}
}
