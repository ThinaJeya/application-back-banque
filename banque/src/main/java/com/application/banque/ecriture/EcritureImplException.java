package com.application.banque.ecriture;

public class EcritureImplException extends Exception{

	public enum MessageEcritureException {
		LISTENULLEOUVIDE("Liste de clients retournée : nulle ou vide"),
		COMPTENEXISTEPAS("Le no_compte entré n'existe pas dans la table Compte"),
		CREDITETDEBITNULL("Le crédit et le débit ne peuvent pas être null"),
		PARAMETREENTREINCORRECT("Le paramètre entré est incorrect ou n'existe pas"),
		ERREURCREATIONOBJET("Un des paramètres de l'objet créée est incorrect");

		private String message;

		private MessageEcritureException(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}

	public EcritureImplException(String messageException) {
		super(messageException);
	}
}
