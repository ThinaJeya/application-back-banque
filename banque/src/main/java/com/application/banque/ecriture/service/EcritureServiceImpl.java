package com.application.banque.ecriture.service;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.application.banque.compte.CompteImplException;
import com.application.banque.compte.CompteImplException.MessageCompteException;
import com.application.banque.compte.service.CompteService;
import com.application.banque.ecriture.EcritureImplException;
import com.application.banque.ecriture.EcritureImplException.MessageEcritureException;
import com.application.banque.ecriture.bean.Ecriture;
import com.application.banque.ecriture.dao.EcritureDaoService;
import com.application.banque.utilitaires.Utilitaires;

/**
 * Implementation d'Ecriture Service
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class EcritureServiceImpl implements EcritureService {

	@Autowired
	private EcritureDaoService ecritureDaoService;

	@Autowired
	private CompteService compteService;

	@Override
	public List<Ecriture> afficherToutesLesEcritures() {
		return ecritureDaoService.findAll();
	}

	@Override
	public List<Ecriture> afficherEcrituresCompte(int noCompte) throws EcritureImplException, CompteImplException {
		List<Ecriture> resultats = new ArrayList<>();
		List<Ecriture> ecritures = afficherToutesLesEcritures();

		if(noCompte > 0 && Utilitaires.noCompteExiste(noCompte, compteService.afficherTousLesComptes())) {
			if(!ObjectUtils.isEmpty(ecritures)) {
				for(Ecriture ecriture : ecritures) {
					if(ecriture.getNoCompte() == noCompte) {
						resultats.add(ecriture);
					}
				}
			} else {
				throw new EcritureImplException(MessageEcritureException.LISTENULLEOUVIDE.getMessage());
			}

		} else {
			throw new EcritureImplException(MessageEcritureException.COMPTENEXISTEPAS.getMessage());
		}

		return resultats;
	}

	@Override
	public List<Ecriture> afficherEcrituresSelonDate(Date dateEcriture) throws EcritureImplException {
		List<Ecriture> resultats = new ArrayList<>();
		List<Ecriture> ecritures = afficherToutesLesEcritures();

		if(dateEcriture != null) {
			if(!ObjectUtils.isEmpty(ecritures)) {
				for(Ecriture ecriture : ecritures) {
					if(ecriture.getDateEcriture().compareTo(dateEcriture) == 0) {
						resultats.add(ecriture);
					}
				}
			} else {
				throw new EcritureImplException(MessageEcritureException.LISTENULLEOUVIDE.getMessage());
			}
		} else {
			throw new EcritureImplException(MessageEcritureException.PARAMETREENTREINCORRECT.getMessage());
		}

		return resultats;
	}

	@Override
	public List<Ecriture> afficherEcrituresEntreDeuxDates(Date dateEcritureDebut, Date dateEcritureFin) throws EcritureImplException {
		List<Ecriture> resultats = new ArrayList<>();
		List<Ecriture> ecritures = afficherToutesLesEcritures();

		if(dateEcritureDebut != null && dateEcritureFin != null && !ObjectUtils.isEmpty(ecritures)) {
			for(Ecriture ecriture : ecritures) {
				if(ecriture.getDateEcriture().compareTo(dateEcritureDebut) >= 0 && ecriture.getDateEcriture().compareTo(dateEcritureFin) <=0) {
					resultats.add(ecriture);
				}
			}
		} else {
			throw new EcritureImplException(MessageEcritureException.LISTENULLEOUVIDE.getMessage());
		}
		return resultats;
	}

	@Override
	public String afficherSoldeCompte(int noCompte) throws EcritureImplException, CompteImplException {
		double solde = 0;
		DecimalFormat df = new DecimalFormat("#.##");

		if(noCompte > 0) {
			List<Ecriture> ecrituresCompte = afficherEcrituresCompte(noCompte);

			if(!ObjectUtils.isEmpty(ecrituresCompte)){
				for(Ecriture ecriture : ecrituresCompte) {
					if(ecriture.getCredit() != null) {
						solde += ecriture.getCredit();
					} else if(ecriture.getDebit() != null) {
						solde -= ecriture.getDebit();
					}
				}
			}			
		} else {
			throw new EcritureImplException(MessageEcritureException.PARAMETREENTREINCORRECT.getMessage());
		}

		return df.format(solde);
	}

	@Override
	public void creerEcriture(Ecriture ecriture) throws EcritureImplException, CompteImplException {

		if(!ObjectUtils.isEmpty(ecriture)) {
			if(Utilitaires.noCompteExiste(ecriture.getNoCompte(), compteService.afficherTousLesComptes())) {
				if(ObjectUtils.isEmpty(ecriture.getCredit()) && ObjectUtils.isEmpty(ecriture.getDebit())) {
					throw new EcritureImplException(MessageEcritureException.CREDITETDEBITNULL.getMessage());
				} else {
					ecritureDaoService.save(ecriture);
				}	
			} else {
				throw new CompteImplException(MessageCompteException.COMPTERETOURNENULL.getMessage());
			}
		} else {
			throw new EcritureImplException(MessageEcritureException.ERREURCREATIONOBJET.getMessage());
		}

	}
}
