package com.application.banque.ecriture.service;

import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.application.banque.compte.CompteImplException;
import com.application.banque.ecriture.EcritureImplException;
import com.application.banque.ecriture.bean.Ecriture;

/**
 * Service d'Ecriture
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface EcritureService {

	/**
	 * Service pour afficher toutes les écritures
	 * @return Liste de toutes les écritures
	 */
	List<Ecriture> afficherToutesLesEcritures();

	/**
	 * Service pour afficher les écritures associées au noCompte entré en paramètre
	 * @param noCompte
	 * @return Liste des écritures associées au noCompte entré en paramètre
	 * @throws EcritureImplException
	 * @throws CompteImplException
	 */
	List<Ecriture> afficherEcrituresCompte(int noCompte) throws EcritureImplException, CompteImplException;

	/**
	 * Service pour afficher les écritures enregistrées à la date entrée en paramètre
	 * @param dateEcriture
	 * @return Liste des écritures enregistrées à la date entrée en paramètre
	 * @throws EcritureImplException
	 */
	List<Ecriture> afficherEcrituresSelonDate(Date dateEcriture) throws EcritureImplException;

	/**
	 * Service pour afficher les écritures enregistrées entre les deux dates entrées en paramètre
	 * @param dateEcritureDebut
	 * @param dateEcritureFin
	 * @return Liste des écritures enregistrées entre les deux dates entrées en paramètre
	 * @throws EcritureImplException 
	 */
	List<Ecriture> afficherEcrituresEntreDeuxDates(Date dateEcritureDebut, Date dateEcritureFin) throws EcritureImplException;

	/**
	 * Service pour afficher le solde du compte associé au noCompte entré en paramètre
	 * @param noCompte
	 * @return solde du compte associé au noCompte entré en paramètre
	 * @throws EcritureImplException
	 * @throws CompteImplException
	 */
	String afficherSoldeCompte(int noCompte) throws EcritureImplException, CompteImplException;

	/**
	 * Service pour créer une nouvelle écriture
	 * @param ecriture
	 * @throws EcritureImplException
	 * @throws CompteImplException
	 */
	void creerEcriture(Ecriture ecriture) throws EcritureImplException, CompteImplException;

}
