package com.application.banque.produit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.application.banque.produit.ProduitImplException;
import com.application.banque.produit.bean.Produit;
import com.application.banque.produit.service.ProduitService;

/**
 * Controller de Produit
 * @author Pirathina Jeyakumar
 *
 */
@RestController
@RequestMapping("/produits")
public class ProduitController {

	@Autowired
	private ProduitService produitService;

	/**
	 * Controller pour afficher tous les produits
	 * @return Liste de tous les produits
	 */
	@GetMapping(path = "/tousLesProduits")
	public List<Produit> afficherTousLesProduits(){
		return produitService.afficherTousLesProduits();
	}

	/**
	 * Controller pour afficher le produit associé au codeProduit entré en paramètre
	 * @param codeProduit
	 * @return produit associé au codeProduit entré en paramètre
	 * @throws ProduitImplException 
	 */
	@GetMapping(path = "/produit/{codeProduit}")
	public Produit afficherProduitSelonCodeProduit(@PathVariable int codeProduit) throws ProduitImplException {
		return produitService.afficherProduitSelonCodeProduit(codeProduit);
	}

	/**
	 * Controller pour afficher le(s) produit(s) selon la désignation entrée en paramètre
	 * @param designation
	 * @return Produit(s) selon la désignation entrée en paramètre
	 * @throws ProduitImplException
	 */
	@GetMapping(path = "/produitParDesignation/{designation}")
	public List<Produit> afficherProduitSelonDesignation(@PathVariable String designation) throws ProduitImplException {
		return produitService.afficherProduitSelonDesignation(designation);
	}

	/**
	 * Controller pour créer un nouveau produit
	 * @param produit
	 * @throws ProduitImplException
	 */
	@PostMapping(path = "/creerProduit")
	public void creerProduit(@RequestBody Produit produit) throws ProduitImplException {
		produitService.creerProduit(produit);
	}

	/**
	 * Controller pour modifier la désignation du produit associé au codeProduit entré en paramètre
	 * @param codeProduit
	 * @param designation
	 * @throws ProduitImplException
	 */
	@PostMapping(path = "/modifierProduit/{codeProduit}")
	public void modifierProduit(@PathVariable int codeProduit, @RequestBody String designation) throws ProduitImplException {
		produitService.modifierProduit(codeProduit, designation);
	}

	/**
	 * Controller pour supprimer le produit associé au codeProduit entré en paramètre
	 * @param codeProduit
	 * @throws ProduitImplException 
	 */
	@DeleteMapping(path = "/supprimerProduit/{codeProduit}")
	public void supprimerProduit(@PathVariable int codeProduit) throws ProduitImplException {
		produitService.supprimerProduit(codeProduit);
	}
}
