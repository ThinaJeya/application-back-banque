package com.application.banque.produit.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Couche Bean de Produit
 * @author Pirathina Jeyakumar
 *
 */
@Entity
@Table(name = "Produit")
public class Produit {

	/**
	 * Code produit
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "code_produit")
	int codeProduit;

	/**
	 * Désignation / libellé du produit
	 */
	@Column(name = "designation")
	String designation;

	public Produit() {
		super();
	}

	public Produit(String designation) {
		super();
		this.designation = designation;
	}

	/**
	 * Permet d'obtenir le code produit
	 * @return code produit
	 */
	public int getCodeProduit() {
		return codeProduit;
	}

	/**
	 * Permet de modifier le code produit
	 * @param codeProduit
	 */
	public void setCodeProduit(int codeProduit) {
		this.codeProduit = codeProduit;
	}

	/**
	 * Permet d'obtenir la désignation / libellé du produit
	 * @return désignation / libellé du produit
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * Permet de modifier la désignation / libellé du produit
	 * @param designation
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	@Override
	public String toString() {
		return "Produit [getCodeProduit()=" + getCodeProduit() + ", getDesignation()=" + getDesignation() + "]";
	}
}
