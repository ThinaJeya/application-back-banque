package com.application.banque.produit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.application.banque.produit.bean.Produit;

/**
 * Interface Dao de Produit Service
 * @author Pirathina Jeyakumar
 *
 */
@Repository
public interface ProduitDaoService extends JpaRepository<Produit, Integer>{

}
