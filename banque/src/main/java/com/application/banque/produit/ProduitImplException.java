package com.application.banque.produit;

public class ProduitImplException extends Exception{

	public enum MessageProduitException{
		PARAMETREENTREINCORRECT("Le paramètre entré est incorrect"),
		LISTENULLEOUVIDE("Liste de clients retournée : nulle ou vide"), 
		CHAMPPRODUITNULLOUVIDE("Un ou plusieurs champs produit null ou vide"),
		PRODUITNEXISTEPAS("Le produit n'existe pas"),
		ERREURCREATIONOBJET("Un des paramètres de l'objet créée est incorrect");;

		private String message;

		private MessageProduitException(String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}

	public ProduitImplException(String messageException) {
		super(messageException);
	}
}
