package com.application.banque.produit.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.application.banque.produit.ProduitImplException;
import com.application.banque.produit.ProduitImplException.MessageProduitException;
import com.application.banque.produit.bean.Produit;
import com.application.banque.produit.dao.ProduitDaoService;
import com.application.banque.utilitaires.Utilitaires;

/**
 * Implémentation de Produit Service
 * @author Pirathina Jeyakumar
 *
 */
@Component
public class ProduitServiceImpl implements ProduitService{
	
	@Autowired
	private ProduitDaoService produitDaoService;

	@Override
	public List<Produit> afficherTousLesProduits() {
		return produitDaoService.findAll();
	}

	@Override
	public Produit afficherProduitSelonCodeProduit(int codeProduit) throws ProduitImplException {
		Optional<Produit> resultat = null;
		
		if(codeProduit > 0) {
			resultat = produitDaoService.findById(codeProduit);
		} else {
			throw new ProduitImplException(MessageProduitException.PARAMETREENTREINCORRECT.getMessage());
		}
		return resultat.orElseThrow(() -> new ProduitImplException(MessageProduitException.PRODUITNEXISTEPAS.getMessage()));
	}

	@Override
	public List<Produit> afficherProduitSelonDesignation(String designation) throws ProduitImplException{
		List<Produit> resultats = new ArrayList<>();
		
		if(!StringUtils.isBlank(designation)) {
			List<Produit> produits = afficherTousLesProduits();
			
			if(!ObjectUtils.isEmpty(produits)) {
				for(Produit produit: produits) {
					if (produit.getDesignation().toUpperCase().contains(designation.toUpperCase())) {
						Produit produitReponse = produit;
						resultats.add(produitReponse);
					}
				}
			} else {
				throw new ProduitImplException(MessageProduitException.LISTENULLEOUVIDE.getMessage());
			}
			
		} else {
			throw new ProduitImplException(MessageProduitException.PARAMETREENTREINCORRECT.getMessage());
		}
		return resultats;
	}

	@Override
	public void creerProduit(Produit produit) throws ProduitImplException {
		
		if(!StringUtils.isBlank(produit.getDesignation())) {
			produit.setDesignation(produit.getDesignation().substring(0, 1).toUpperCase() + produit.getDesignation().substring(1));
			produitDaoService.save(produit);
		} else {
			throw new ProduitImplException(MessageProduitException.ERREURCREATIONOBJET.getMessage());
		}
	}

	@Override
	public void modifierProduit(int codeProduit, String designation) throws ProduitImplException {
		List<Produit> produits = afficherTousLesProduits();
		
		if(codeProduit > 0 && !StringUtils.isBlank(designation)) {
			if(!ObjectUtils.isEmpty(produits) && Utilitaires.codeProduitExiste(codeProduit, produits)) {
				String designationUpperCase = designation.substring(0, 1).toUpperCase() + designation.substring(1);
				for(Produit produit:produits) {
					if(produit.getCodeProduit() == codeProduit) {
						produit.setDesignation(designationUpperCase);
						produitDaoService.save(produit);
					}
				}
			} else {
				throw new ProduitImplException(MessageProduitException.PRODUITNEXISTEPAS.getMessage());
			}	
		} else {
			throw new ProduitImplException(MessageProduitException.PARAMETREENTREINCORRECT.getMessage());
		}
	}

	@Override
	public void supprimerProduit(int codeProduit) throws ProduitImplException {
		if(codeProduit > 0) {
			if(Utilitaires.codeProduitExiste(codeProduit, afficherTousLesProduits())) {
				produitDaoService.deleteById(codeProduit);				
			} else {
				throw new ProduitImplException(MessageProduitException.PRODUITNEXISTEPAS.getMessage());
			}
			
		} else {
			throw new ProduitImplException(MessageProduitException.PARAMETREENTREINCORRECT.getMessage());
		}
	}
}
