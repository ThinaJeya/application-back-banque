package com.application.banque.produit.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.application.banque.produit.ProduitImplException;
import com.application.banque.produit.bean.Produit;

/**
 * Service de Produit
 * @author Pirathina Jeyakumar
 *
 */
@Service
public interface ProduitService {

	/**
	 * Service pour afficher tous les produits
	 * @return Liste de tous les produits
	 */
	public List<Produit> afficherTousLesProduits();

	/**
	 * Service pour afficher le produit associé au codeProduit entré en paramètre
	 * @param codeProduit
	 * @return produit associé au codeProduit entré en paramètre
	 * @throws ProduitImplException 
	 */
	public Produit afficherProduitSelonCodeProduit(int codeProduit) throws ProduitImplException;

	/**
	 * Service pour afficher le(s) produit(s) selon la désignation entrée en paramètre
	 * @param designation
	 * @return Produit(s) selon la désignation entrée en paramètre
	 * @throws ProduitImplException
	 */
	public List<Produit> afficherProduitSelonDesignation(String designation) throws ProduitImplException;

	/**
	 * Service pour créer un nouveau produit
	 * @param produit
	 * @throws ProduitImplException
	 */
	public void creerProduit(Produit produit) throws ProduitImplException;

	/**
	 * Service pour modifier la désignation du produit associé au codeProduit entré en paramètre
	 * @param codeProduit
	 * @param designation
	 * @throws ProduitImplException
	 */
	public void modifierProduit(int codeProduit, String designation) throws ProduitImplException;

	/**
	 * Service pour supprimer le produit associé au codeProduit entré en paramètre
	 * @param codeProduit
	 * @throws ProduitImplException 
	 */
	public void supprimerProduit(int codeProduit) throws ProduitImplException;






}
