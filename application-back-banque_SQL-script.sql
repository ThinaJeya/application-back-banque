/* 
** application_back_banque_script-SQL.sql
**
** Script SQL de la base de données du projet application-back-banque
**
** Pirathina Jeyakumar
*/

-- Création des tables.

CREATE TABLE Client
(
    Code_Client     SERIAL,
    Nom             CHARACTER VARYING(32)   NOT NULL,
    Prenom          CHARACTER VARYING(32)   NOT NULL,
    Date_Naissance  DATE                    NOT NULL,
    Adresse         CHARACTER VARYING(64)   NOT NULL,
    Code_Postal     CHARACTER(5)            NOT NULL,
    Ville           CHARACTER VARYING(32)   NOT NULL,
    Telephone       CHARACTER(10)               NULL,
    Email           CHARACTER VARYING(64)       NULL,
    PRIMARY KEY(Code_Client)
);

CREATE TABLE Compte
(
    No_Compte       SERIAL,
    Date_Creation   DATE                    NOT NULL,
    Code_Client     INTEGER                 NOT NULL,
    PRIMARY KEY(No_Compte),
    FOREIGN KEY(Code_Client) REFERENCES Client(Code_Client)
);

CREATE TABLE Ecriture
(
    No_Ecriture     SERIAL,
    Date_Ecriture   DATE                    NOT NULL,
    Debit           NUMERIC(13,2)               NULL,
    Credit          NUMERIC(13,2)               NULL,
    No_Compte       INTEGER                 NOT NULL,
    PRIMARY KEY(No_Ecriture),
    FOREIGN KEY(No_Compte) REFERENCES Compte(No_Compte)
);

CREATE TABLE Produit
(
    Code_Produit    SERIAL,
    Designation     CHARACTER VARYING(64)   NOT NULL,
    PRIMARY KEY(Code_Produit)
);

CREATE TABLE Associer
(
    No_Compte       INTEGER                 NOT NULL,
    Code_Produit    INTEGER                 NOT NULL,
    PRIMARY KEY(No_Compte,Code_Produit),
    FOREIGN KEY(No_Compte) REFERENCES Compte(No_Compte),
    FOREIGN KEY(Code_Produit) REFERENCES Produit(Code_Produit)
);

-- Table Client.

INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (1, 'Winch', 'Largo', '1969-08-15', '21 Dollard Street', '00521', 'New York', '0059456579', 'Largo.Winch@GroupeW.com');
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (2, 'Blake', 'Francis', '1965-06-28', 'Scotland Yard 21 Regent Street', '1159 ', 'Londres', '0055555500', 'Francis.Blake@is.gouv.uk');
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (3, 'Legrand', 'Bob', '1958-12-12', '56 Rue Picasso', '31400', 'Toulouse', '0552498547', NULL);
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (4, 'Mortimer', 'Philip', '1968-11-02', '99 bis Park Lane', '10060', 'Londres', '9956458978', NULL);
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (5, 'Moselle', 'Didier', '1970-04-02', '33 Avenue de la Porte Blanche', '75015', 'Paris', NULL, 'Didier.Moselle@fondation-meyer.org');
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (6, 'Tsuno', 'Yoko', '1973-04-08', '28 Boulevard du Marais', '75022', 'Paris', NULL, NULL);
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (7, 'Winch', 'Nerio', '1932-06-14', '21 Dollard Street', '00521', 'New York', '0059456578', 'Nerio.Winch@GroupeW.com');
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (8, 'Legrand', 'Bob', '1968-08-23', '26 Wall Street', '00759', 'New York', NULL, NULL);
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (11, 'Pepere', 'Kiwi', '1994-05-11', '8956 Seventh Avenue', '00521', 'New York', '0226939481', NULL);
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (12, 'Memere', 'Rick', '1989-01-04', '8956 tenth Avenue', '00369', 'New York', '0066228844', NULL);
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (13, 'Bohemian', 'Gypsy', '1992-01-20', '6953 Fifth Avenue', '00695', 'New York', '0369548941', NULL);
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (15, 'Sacquet', 'Bilbon', '1956-08-21', '5681, Chemin du champs de tournesol', '99550', 'Colline-sur-Montagne', '8899112233', NULL);
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (10, 'Deschamps', 'Morty', '1991-08-08', '5415 fifth Avenue', '00521', 'New York', '8899112244', 'Morty.Deschamps@hotmail.Com');
INSERT INTO public.client (code_client, nom, prenom, date_naissance, adresse, code_postal, ville, telephone, email)
VALUES (14, 'Sacquet', 'Frodon', '1989-05-06', '5681, Chemin du champs de tournesol', '99550', 'Colline-sur-Montagne', '6633554400', 'Frodon.Sacquet.Perso@communaute-anneau.Com');


-- Table Compte.

INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (1, '1956-09-26', 7);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (2, '1976-02-28', 1);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (3, '1988-02-21', 5);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (4, '1988-12-01', 2);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (5, '1992-10-21', 6);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (6, '1996-10-28', 4);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (7, '1997-09-15', 1);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (8, '1999-05-17', 2);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (9, '2000-07-28', 3);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (10, '2000-11-05', 8);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (11, '2001-10-09', 5);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (12, '2012-12-12', 13);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (13, '2006-01-20', 12);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (14, '2020-12-15', 14);
INSERT INTO public.compte (no_compte, date_creation, code_client) VALUES (15, '2020-12-16', 10);

-- Table Ecriture.

INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (1, '1956-09-26', NULL, 2500000.00, 1);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (2, '1976-02-28', NULL, 1000000.00, 2);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (3, '1976-11-02', NULL, 3000000.00, 1);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (4, '1988-02-21', NULL, 200000.00, 3);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (5, '1988-04-07', 250000.00, NULL, 2);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (6, '1988-05-06', 15000.00, NULL, 3);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (7, '1988-12-01', NULL, 500000.00, 4);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (8, '1989-03-29', NULL, 2500000.00, 1);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (9, '1989-12-24', 1250.50, NULL, 3);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (10, '1991-03-07', 20000.00, NULL, 3);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (11, '1992-07-21', NULL, 500000.00, 4);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (12, '1992-09-23', NULL, 5000000.00, 1);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (13, '1992-10-21', NULL, 300.00, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (14, '1992-11-14', NULL, 500.00, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (15, '1992-11-22', 125.00, NULL, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (16, '1993-02-08', 99.99, NULL, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (17, '1993-05-07', NULL, 500.00, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (18, '1994-01-20', 1333.33, NULL, 3);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (19, '1994-12-15', 56.59, NULL, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (20, '1995-02-22', 1500.25, NULL, 3);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (21, '1996-08-19', 254.45, NULL, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (22, '1996-10-03', 32.55, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (23, '1996-10-16', NULL, 500000.00, 4);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (24, '1996-10-28', NULL, 500.00, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (25, '1996-11-14', 64.25, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (26, '1996-11-24', 100.00, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (27, '1996-12-26', 36.66, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (28, '1997-02-07', 17.20, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (29, '1997-03-09', 19.45, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (30, '1997-06-08', 64.79, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (31, '1997-06-20', 86.43, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (32, '1997-07-26', NULL, 200000.00, 3);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (33, '1997-09-15', NULL, 55000000.00, 7);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (34, '1997-10-19', 84.12, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (35, '1997-11-15', 555.50, NULL, 3);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (36, '1997-12-06', 10000000.00, NULL, 7);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (37, '1997-12-31', 234.50, NULL, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (38, '1998-03-09', 5555000.00, NULL, 7);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (39, '1998-03-16', 5.65, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (40, '1998-04-27', 70.01, NULL, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (41, '1998-06-08', 30.55, NULL, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (42, '1998-07-19', 15.15, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (43, '1998-07-22', NULL, 800.00, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (44, '1998-08-13', 13.78, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (45, '1998-08-18', 21.80, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (46, '1998-09-10', NULL, 500000.00, 2);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (47, '1999-02-19', 102.36, NULL, 3);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (48, '1999-02-19', 54.56, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (49, '1999-04-11', 54.44, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (50, '1999-04-28', 69.54, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (51, '1999-05-17', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (52, '1999-06-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (53, '1999-06-08', 3.35, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (54, '1999-06-14', 57.51, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (55, '1999-06-22', NULL, 500000.00, 4);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (56, '1999-07-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (57, '1999-08-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (58, '1999-08-20', NULL, 2500000.00, 7);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (59, '1999-09-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (60, '1999-10-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (61, '1999-11-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (62, '1999-11-10', NULL, 100.00, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (63, '1999-12-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (64, '2000-01-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (65, '2000-02-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (66, '2000-03-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (67, '2000-04-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (68, '2000-04-13', 359.99, NULL, 3);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (69, '2000-05-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (70, '2000-05-04', NULL, 2500000.00, 7);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (71, '2000-06-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (72, '2000-06-18', 1.11, NULL, 6);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (73, '2000-07-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (74, '2000-07-09', 1250000.00, NULL, 1);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (75, '2000-07-12', 29.99, NULL, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (76, '2000-07-28', NULL, 2500.00, 9);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (77, '2000-07-30', 132.25, NULL, 9);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (78, '2000-08-02', NULL, 450.00, 8);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (79, '2000-08-17', 135.56, NULL, 9);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (80, '2000-08-24', 684.50, NULL, 9);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (81, '2000-09-03', 399.05, NULL, 9);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (82, '2000-10-15', 12.55, NULL, 3);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (83, '2000-10-24', 157.00, NULL, 9);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (84, '2000-10-25', 298.69, NULL, 9);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (85, '2000-10-26', 397.00, NULL, 9);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (86, '2000-11-05', NULL, 1500.00, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (87, '2000-11-13', 102.55, NULL, 9);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (88, '2000-11-17', 125.89, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (89, '2000-12-12', 35.98, NULL, 9);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (90, '2000-12-13', 56.77, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (91, '2000-12-19', 546.55, NULL, 9);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (92, '2000-12-21', 289.25, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (93, '2000-12-25', 859.00, NULL, 9);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (94, '2001-01-20', 432.25, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (95, '2001-01-31', 41.00, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (96, '2001-02-02', 98.89, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (97, '2001-02-27', NULL, 2500000.00, 7);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (98, '2001-02-28', 102.55, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (99, '2001-03-16', 78.60, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (100, '2001-04-17', 28.01, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (101, '2001-05-11', NULL, 1500.00, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (102, '2001-05-16', 500.32, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (103, '2001-03-02', 52000.00, NULL, 2);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (104, '2001-06-24', 328.59, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (105, '2001-07-11', 100.00, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (106, '2001-08-07', 12.55, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (107, '2001-08-18', 600.50, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (108, '2001-08-27', 222.00, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (109, '2001-09-04', 79.00, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (110, '2001-09-09', NULL, 10000000.00, 11);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (111, '2001-09-12', NULL, 1500.00, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (112, '2001-09-19', 28.55, NULL, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (113, '2001-10-10', 200.05, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (114, '2001-10-11', 358.45, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (115, '2001-10-18', 299.99, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (116, '2001-10-21', NULL, 125000.00, 7);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (117, '2001-11-15', 46.50, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (118, '2001-11-27', 58.88, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (119, '2001-12-05', 125000.00, NULL, 2);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (120, '2001-12-06', 456.79, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (121, '2001-12-22', 12.55, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (122, '2001-12-29', 36.99, NULL, 10);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (123, '2020-11-23', NULL, 356.23, 12);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (124, '2020-11-23', NULL, 356.23, 12);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (125, '2020-11-23', NULL, 356.23, 12);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (126, '2020-11-23', 5000.23, NULL, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (127, '2020-11-23', NULL, 6325.36, 5);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (128, '2020-12-15', NULL, 125000.00, 14);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (129, '2020-12-15', 56.25, NULL, 14);
INSERT INTO public.ecriture (no_ecriture, date_ecriture, debit, credit, no_compte) VALUES (130, '2020-12-15', 56.25, NULL, 12);

-- Table Produits.

INSERT INTO public.produit (code_produit, designation) VALUES (7, 'Consultation - Minitel');
INSERT INTO public.produit (code_produit, designation) VALUES (8, 'Consultation - Internet');
INSERT INTO public.produit (code_produit, designation) VALUES (9, 'Consultation - SMS');
INSERT INTO public.produit (code_produit, designation) VALUES (1, 'Chèque');
INSERT INTO public.produit (code_produit, designation) VALUES (2, 'Carte Bancaire - Paiement Immédiat');
INSERT INTO public.produit (code_produit, designation) VALUES (3, 'Carte Bancaire - Paiement Différé');
INSERT INTO public.produit (code_produit, designation) VALUES (4, 'Assurance - Sécurité Plus');
INSERT INTO public.produit (code_produit, designation) VALUES (5, 'Assurance - Sécurité Plus Plus');
INSERT INTO public.produit (code_produit, designation) VALUES (6, 'Consultation - Téléphonique');
INSERT INTO public.produit (code_produit, designation) VALUES (10, 'Virement - Internet');
INSERT INTO public.produit (code_produit, designation) VALUES (11, 'Assurance - couverture maximum achats');
INSERT INTO public.produit (code_produit, designation) VALUES (12, 'Mastercard - Gold');

-- Table Associer.

INSERT INTO public.associer (no_compte, code_produit) VALUES (1, 1);
INSERT INTO public.associer (no_compte, code_produit) VALUES (1, 3);
INSERT INTO public.associer (no_compte, code_produit) VALUES (1, 5);
INSERT INTO public.associer (no_compte, code_produit) VALUES (1, 6);
INSERT INTO public.associer (no_compte, code_produit) VALUES (2, 1);
INSERT INTO public.associer (no_compte, code_produit) VALUES (2, 2);
INSERT INTO public.associer (no_compte, code_produit) VALUES (2, 4);
INSERT INTO public.associer (no_compte, code_produit) VALUES (2, 6);
INSERT INTO public.associer (no_compte, code_produit) VALUES (2, 8);
INSERT INTO public.associer (no_compte, code_produit) VALUES (2, 9);
INSERT INTO public.associer (no_compte, code_produit) VALUES (3, 1);
INSERT INTO public.associer (no_compte, code_produit) VALUES (3, 3);
INSERT INTO public.associer (no_compte, code_produit) VALUES (3, 4);
INSERT INTO public.associer (no_compte, code_produit) VALUES (3, 7);
INSERT INTO public.associer (no_compte, code_produit) VALUES (3, 8);
INSERT INTO public.associer (no_compte, code_produit) VALUES (3, 9);
INSERT INTO public.associer (no_compte, code_produit) VALUES (4, 1);
INSERT INTO public.associer (no_compte, code_produit) VALUES (4, 3);
INSERT INTO public.associer (no_compte, code_produit) VALUES (4, 6);
INSERT INTO public.associer (no_compte, code_produit) VALUES (5, 1);
INSERT INTO public.associer (no_compte, code_produit) VALUES (5, 3);
INSERT INTO public.associer (no_compte, code_produit) VALUES (5, 5);
INSERT INTO public.associer (no_compte, code_produit) VALUES (5, 6);
INSERT INTO public.associer (no_compte, code_produit) VALUES (5, 8);
INSERT INTO public.associer (no_compte, code_produit) VALUES (5, 9);
INSERT INTO public.associer (no_compte, code_produit) VALUES (6, 1);
INSERT INTO public.associer (no_compte, code_produit) VALUES (6, 2);
INSERT INTO public.associer (no_compte, code_produit) VALUES (6, 5);
INSERT INTO public.associer (no_compte, code_produit) VALUES (6, 6);
INSERT INTO public.associer (no_compte, code_produit) VALUES (7, 3);
INSERT INTO public.associer (no_compte, code_produit) VALUES (7, 5);
INSERT INTO public.associer (no_compte, code_produit) VALUES (7, 9);
INSERT INTO public.associer (no_compte, code_produit) VALUES (8, 1);
INSERT INTO public.associer (no_compte, code_produit) VALUES (8, 4);
INSERT INTO public.associer (no_compte, code_produit) VALUES (8, 6);
INSERT INTO public.associer (no_compte, code_produit) VALUES (9, 1);
INSERT INTO public.associer (no_compte, code_produit) VALUES (9, 3);
INSERT INTO public.associer (no_compte, code_produit) VALUES (10, 1);
INSERT INTO public.associer (no_compte, code_produit) VALUES (10, 2);
INSERT INTO public.associer (no_compte, code_produit) VALUES (10, 4);
INSERT INTO public.associer (no_compte, code_produit) VALUES (11, 3);
INSERT INTO public.associer (no_compte, code_produit) VALUES (1, 2);
INSERT INTO public.associer (no_compte, code_produit) VALUES (11, 5);
INSERT INTO public.associer (no_compte, code_produit) VALUES (14, 9);

-- Droits.

GRANT SELECT ON Client      TO public;
GRANT SELECT ON Compte      TO public;
GRANT SELECT ON Ecriture    TO public;
GRANT SELECT ON Produit     TO public;
GRANT SELECT ON Associer    TO public;

-- Démarrage de l'auto-incrément aux numéros souhaités.

-- Table Client.
ALTER SEQUENCE client_code_client_seq RESTART WITH 16;

-- Table Compte.
ALTER SEQUENCE compte_no_compte_seq RESTART WITH 16;

-- Table Ecriture.
ALTER SEQUENCE ecriture_no_ecriture_seq RESTART WITH 131;

-- Table Produit.
ALTER SEQUENCE produit_code_produit_seq RESTART WITH 13;