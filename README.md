# Application back banque

## README du projet application-back-banque de Pirathina Boisson.

application-back-banque est un projet back-end développé en Java / SpringBoot / Postgresql de simulation de 
gestion de comptes bancaires. 
L’objectif du projet était de proposer un back-end optimisé sur une approche MVC pour la manipulation des données (lecture, création, modification, suppression).

Le projet est composé de trois parties décrites ci-dessous :

1) Back-end

* Langage : Java 1.8
* Framework : SpringBoot 2.4.1
* Design pattern : MVC (Modèle-Vue-Controller)
* Librairies / Frameworks additionnel(le)s : JUnit 4.13.1 ; Mockito 3.6.0
* Outils autres : SonarLint

2) Requêtes Postman

Utilisation de l'outil Postman pour paramétrer et tester les requêtes http (GET, POST, DELETE) sur l'API REST exposée par les controllers.

3) La base de données

La construction de la base de données s'est appuyée sur les bonnes pratiques de conception en vigueur (analyse des besoins du projet ; traduction des entités en relation ; détermination des associations entre les relations).

Le modèle relationnel des données de la base de données est le suivant (clés primaires en gras) :

*Client* (**Code_Client**, Nom, Prenom, Date_naissance, Adresse, Code_Postal, Ville, Telephone, Email) 

*Ecriture* (**No_Ecriture**, Date_Ecriture, Debit, Credit, #No_Compte) 

*Produit* (**Code_Produit**, Designation) 

*Associer* (**#Code_Produit**, **#No_Compte**) 

*Compte* (**No_Compte**, Date_Creation, #Code_Client)

***********************************************************************************
***Instructions d’import du projet application-back-banque de Pirathina Boisson.***
***********************************************************************************

1) Import de la base de données

Prérequis : Avoir Postgresql et pgAdmin sur son poste

* Dans pgAdmin : créer une nouvelle base de données nommée « comptesdb » (nom entré dans application.properties du back-end)
* Une fois la base créée, ouvrir le query tool sur la base, puis sur open file -> ouvrir le fichier « application-back-banque_SQL-script.sql ».
* Exécuter le script. La base de données sera créée.

2) Import du back-end

Prérequis : Avoir un IDE sur son poste (Eclipse), jdk minimum : 1.8

* Aller sur gitLab : [https://gitlab.com/ThinaJeya/application-back-banque](https://gitlab.com/ThinaJeya/application-back-banque) et télécharger le fichier archive (.zip) ou récupérer le clone du repository : [https://gitlab.com/ThinaJeya/application-back-banque.git](https://gitlab.com/ThinaJeya/application-back-banque.git)
* Importer le projet dans votre IDE favori.
* Configurer le fichier application.properties du projet : Renseigner l’username et le password de votre profil user Postgresql pour connecter la base de données avec le back-end.
* Exécuter le code (run).

3) Import des requêtes Postman

Prérequis : Avoir Postman sur son poste

* Cliquer sur Import et sélectionner le fichier « application-back-banque.postman_collection.json », la collection de requêtes sera importée.

**Les requêtes sont prêtes à l'usage et peuvent être testées une fois que le code du projet aura été exécuté dans l'IDE.**